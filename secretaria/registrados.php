<?php require_once ('conexion.php');
$conexion=conectarBD();


function getevento() {
    $con = conectarBD();

    $query = 'SELECT * from evento where estado= TRUE';
 

    $resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
}

function getlistatuevento($id_evento){
$con=conectarBD();
$query="Select cedula,nombre,apellido,tipo_pago,comprobante,monto from persona where evento= ".$id_evento;
$resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
    
}

$id_evento= isset($_POST['id_evento']) ? $_POST['id_evento'] : '' ;
$data['nombre'] = getevento();
if ($id_evento != ''){
    
    $data['evento'] = getlistatuevento($id_evento);
    
    
}else{
    $data['evento'] ='';
    
};

?>


<html lang="en">
<head>
  <title>Registrados</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.css">
  
  <script src="assets/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
    <li><a href="evento2.php">Evento</a></li>
    <li><a href="registrados.php">Registrados</a></li>
      <li><a href="cliente.php">Registro Cliente</a></li>
      <li><a href="proveedor.php">Registro Proveedor</a></li>
      <li><a href="producto.php">Registro de Producto</a></li>
      <li><a href="recibo.php" target="_blank">Imprimir Recibo</a></li>
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span>Salir</a></li>
    </ul>
  </div>
</nav>
<?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }
        
    ?>
   
     
    
       <div class="container">
       <div class="form-group col-sm-3">
           <form action="registrados.php" method="post">
        <label for=exampleFormControlSelect1" class="h4">Evento</label>
        <select class="form-control" id="exampleFormControlSelect1" id="id_evento"  name="id_evento">
        <?php
        foreach ($data['nombre'] as $d) :
        ?>
            <option class="po" value="<?php echo $d->id_evento; ?>" > <?php echo $d->nombre; ?>  </option>
            <?php endforeach; ?>
              </select>  
                </div>
           <div class="form-group col-sm-4">
           <button type="submit" style="margin-top: 5px" id="listar" class="btn btn-success btn-lg pull-center ">Listar</button>
           </div>
           </form>
            <table class="table table-bordered">
    <thead>
      <tr>
        <th>Cedula</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Forma de Pago</th>
        <th>Comprobante</th>
        <th>Monto</th>
      </tr>
    </thead>
    <tbody>
    
        <?php if ($data['evento'] != ''):
     foreach ($data['evento'] as $d):?>
            <tr>
        <td><?php echo $d->cedula; ?></td>
        <td><?php echo $d->nombre; ?></td>
        <td> <?php echo $d->apellido; ?></td>
        <td><?php echo $d->tipo_pago;?></td>
        <td><?php echo $d->comprobante;?></td>
        <td><?php echo $d->monto; ?></td>
      </tr>
      <?php endforeach;?>
        
          <?php else :  ?>
        <tr>
        <td><?php  ?></td>
        <td><?php  ?></td>
        <td> <?php  ?></td>
        <td><?php ?></td>
        <td><?php ?></td>
        <td><?php ?></td>
      </tr>
       <?php endif;  ?>
    </tbody>
  </table>

    </div>

</body>

</html>
