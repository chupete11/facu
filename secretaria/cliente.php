<?php require_once ('conexion.php');
$conexion=conectarBD();?>

<html lang="en">
<head>
  <title>Cliente</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.css">
  <link rel="stylesheet" href="../css/estilo2.css">
  <script src="../assets/jquery-3.3.1.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>

</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
      
      <li><a href="evento2.php">Evento</a></li>
      <li><a href="registrados.php">Registrados</a></li>
      <li><a href="cliente.php">Registro Cliente</a></li>
      <li><a href="proveedor.php">Registro Proveedor</a></li>
      <li><a href="producto.php">Registro de Productos</a></li>
      <li><a href="recibo.php" target="_blank">Imprimir Recibo</a></li>
     
      <li><a href="https://www.google.com.py/?gws_rd=ssl" target="_blank">Busqueda</a></li>
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="../cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span> Salir</a></li>
    </ul>
  </div>
</nav>
    <div class="container">
        <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }
        
    ?>
    
    <center><h1>Bienvenido Usuario</h1></center>
    <center><?php
    //de esta forma concatenamos el dato de ususario registrado para un saludo personalizado
    echo "Hola: " . $_SESSION["usuario"]. "<br><br>";
        
    ?>
        </center>
  

    </div>
    
       <div class="container">
            <div class="col-sm-12">
                <div class="well" style="margin-top: 15px;">
                    <h1 class="text-center">Registro de Cliente </h1>
                    <b><hr></b>
                    <form action="procesar9.php" method="POST" name="frm">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4">Ruc</label>
                                <input type="text" class="form-control"  id="cedu"  name="cedu" placeholder="Ruc" required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Nombre</label>
                                <input type="text" class="form-control text-danger"  id="nombre"  name="nombre" placeholder="Nombre" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Apellido</label>
                                <input type="text" class="form-control"  id="apellido" name="apellido"  placeholder="Apellido"  required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            
                            
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 ">Email</label>
                                <input class="form-control" name="correo" rows="1" id="correo"  placeholder="Correo"  required></input>
                                <div class="help-block with-errors"></div>
                               
                            </div>
                           
                            
                            <div class="form-group col-sm-4">
                                <label for="text" class="h4">Celular</label>
                                <input type="text" class="form-control"  id="celular" name="celular"  placeholder="Celular" required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4">Telefono</label>
                                <input type="text" class="form-control"  name="telefono"  id="telefono" value="0" placeholder="Telefono" >
                                    <div class="help-block with-errors"></div>
                            </div>
                             <div class="form-group col-xl-10-4">
                                <label for="name" class="h4">Mensaje</label>
                                <input type="textarea" class="form-control"  name="mensaje" id="mensaje" style="width: 350px; height: 100px;" >
                                    <div class="help-block with-errors"></div>
                            </div>
                             <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Fecha de registro</label>
                                <input type="date" class="form-control" id="fecha" name="fecha" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                
                            </div>
                            
                                   
                                    <div class="form-group col-sm-4">
                                        <button type="submit" style="margin-top: 30px" id="enviar" class="btn btn-success btn-lg pull-center ">Registrar</button>
                                    </div>
                                    
                            </form>
                        </div>
                    </div>
                </div>

    </div>

</body>
<footer class="container-fluid text-center">
  <p>Global Running</p>
  <p>Fernando de la Mora, zona Sur</p>
  <p>Telefonos:021555.555 Celular:0981555.555</p>
</footer>
</html>
