<?php
?>
<html>
<head>
    <!-- Latest compiled and minified CSS -->

    

    <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">

<!-- jQuery library -->

<script src="../assets/jquery-3.3.1.min.js"></script>
<script>
localStorage.url = 'http://localhost:8080/runningsys/';
</Script>


<!-- Latest compiled JavaScript -->
<script src="../bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
     <li><a href="evento2.php">Evento</a></li>
     <li><a href="registrados.php">Registrados</a></li>
      <li><a href="cliente.php">Registro Cliente</a></li>
      <li><a href="proveedor.php">Registro Proveedor</a></li>
      <li><a href="producto.php">Registro de Productos</a></li>
        <li><a href="recibo.php" target="_blank">Imprimir Recibo</a></li>
      <li><a href="https://www.google.com.py/?gws_rd=ssl" target="_blank">Busqueda</a></li>
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="login1.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav>
<?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }
        
    ?>
     <div class="container">
            <div class="col-sm-12">
                <div class="well" style="margin-top: 15px;">
                    <h1 class="text-center">Impresion de Recibo</h1>
                    <b><hr></b>
                    <form action="#" method="POST" name="frm">
                        <div class="row">
                            <div class="form-group col-sm-12"> 
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="name" class="h4 text-center">Numero Recibo</label>
                                <input type="text" class="form-control text-danger" name="buscar" placeholder="Buscar"  required>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-info btn-sm" style="margin-top: 40px" id="buscar">
                                            <span class="glyphicon glyphicon-search"></span> Search
                                        </button>
                                    </div>
                                </div>
                                
                                    <div class="help-block with-errors"></div>
                                
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Nombre y Apellido</label>
                                <input type="text" class="form-control" name="nombre" placeholder="Nombre" >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Evento</label>
                                <input type="text" class="form-control" name="evento" placeholder="evento" >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Talla</label>
                                <input type="text" class="form-control" name="talla" placeholder="Talla" >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Cedula</label>
                                <input type="text" class="form-control" name="cedula" placeholder="Cedula" >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Fecha Nac.</label>
                                <input type="text" class="form-control" name="fecha_nac" placeholder="Fecha Nacimiento" >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Monto</label>
                                <input type="text" class="form-control" name="monto" placeholder="Monto" >
                                    <div class="help-block with-errors"></div>
                            </div>

                                    <div class="form-group col-sm-4">
                                    </div>
                                                                <div class="form-group col-sm-4">
                                        <input type="button"  style="margin-top: 5px" id="imprimir" value="Imprimir" class="btn btn-success btn-lg pull-center " disabled>
                                    </div>
                            <div class="form-group col-sm-4">
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
         <script src="../js/impresion.js"></script>
         
</body>

</html>
