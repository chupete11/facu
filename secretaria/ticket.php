<?php


$datos['nombre']  = $_POST["nombre"];
$datos['evento']  = $_POST["evento"];
$datos['talla'] = $_POST["talla"];
$datos['cedula'] = $_POST["cedula"];
$datos['fecha_nac'] = $_POST["fecha_nac"];
$datos['monto'] = $_POST["monto"];


require  '../libs/ticket/autoload.php'; //Nota: si renombraste la carpeta a algo diferente de "ticket" cambia el nombre en esta línea
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

/*
	Este ejemplo imprime un
	ticket de venta desde una impresora térmica
*/


/*
    Aquí, en lugar de "POS" (que es el nombre de mi impresora)
	escribe el nombre de la tuya. Recuerda que debes compartirla
	desde el panel de control
*/

$nombre_impresora = "pos"; 


$connector = new WindowsPrintConnector($nombre_impresora);
$printer = new Printer($connector);
#Mando un numero de respuesta para saber que se conecto correctamente.
echo 1;
/*
	Vamos a imprimir un logotipo
	opcional. Recuerda que esto
	no funcionará en todas las
	impresoras

	Pequeña nota: Es recomendable que la imagen no sea
	transparente (aunque sea png hay que quitar el canal alfa)
	y que tenga una resolución baja. En mi caso
	la imagen que uso es de 250 x 250
*/

# Vamos a alinear al centro lo próximo que imprimamos
$printer->setJustification(Printer::JUSTIFY_CENTER);

/*
	Intentaremos cargar e imprimir
	el logo
*/
$printer->setJustification(Printer::JUSTIFY_CENTER);
$logo = EscposImage::load( "../imagen/logo2.jpg", false);
$printer->bitImage($logo);
/*
try{
	$logo = EscposImage::load("../imagen/logo.png", false);
    $printer->bitImage($logo);
}catch(Exception $e){'No hacemos nada si hay error'}*/

/*
	Ahora vamos a imprimir un encabezado
*/

$printer->text("\n"."Global Running" . "\n");
$printer->text("Direccion: Septima 1076" . "\n");
$printer->text("Tel: 021555.555" . "\n");
#La fecha también
date_default_timezone_set("America/Asuncion");
$printer->text(date("Y-m-d H:i:s") . "\n");
// $printer->text("-----------------------------" . "\n");
$printer->setJustification(Printer::JUSTIFY_LEFT);
// $printer->text("CANT  DESCRIPCION    P.U   IMP.\n");
/*
	Ahora vamos a imprimir los
	productos
*/
	/*Alinear a la izquierda para la cantidad y el nombre*/
	$printer->setJustification(Printer::JUSTIFY_LEFT);
	$printer->text("////////////////////////////////"."\n");
    $printer->text("Nombre y Apellido:\n");
	$printer->text($datos['nombre']."\n");
    $printer->text("Evento: \n");
    $printer->text( $datos['evento']."\n");
    $printer->text("Talla: \n");
	$printer->text( $datos['talla']."\n");
	$printer->text("Cedula: \n");
	$printer->text($datos['cedula']."\n");
	$printer->text("fecha Nacimiento: \n");
	$printer->text($datos['fecha_nac']."\n");
/*
	Terminamos de imprimir
	los productos, ahora va el total
*/
$printer->text("-----------------------------"."\n");
$printer->setJustification(Printer::JUSTIFY_RIGHT);
$printer->text("Monto:". number_format($datos['monto'], 0, ',', '.')." Gs\n");
/*$printer->text("IVA: $16.00\n");
$printer->text("TOTAL: $116.00\n");*/


/*
	Podemos poner también un pie de página
*/
$printer->setJustification(Printer::JUSTIFY_CENTER);
$printer->text("Muchas gracias por su compra\n");



/*Alimentamos el papel 3 veces*/
$printer->feed(3);

/*
	Cortamos el papel. Si nuestra impresora
	no tiene soporte para ello, no generará
	ningún error
*/
$printer->cut();

/*
	Por medio de la impresora mandamos un pulso.
	Esto es útil cuando la tenemos conectada
	por ejemplo a un cajón
*/
$printer->pulse();

/*
	Para imprimir realmente, tenemos que "cerrar"
	la conexión con la impresora. Recuerda incluir esto al final de todos los archivos
*/
$printer->close();

?>