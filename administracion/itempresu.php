<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <script src="../assets/jquery-3.3.1.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <script src="../bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" href="../css/estilo13.css">
        <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
           
    </head>
    <body>
        <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="inicio.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Presupuesto
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="presupuesto1.php">Crear Presupuesto</a></li>
          <li><a href="updatePresu.php">Actualizar Presupuesto</a></li>
          
        </ul>
      </li>
        <li><a href="itempresu.php">Item de Presupuesto</a></li>
        <li><a href="registrados.php">Registrados</a></li>
        <li><a href="mensaje.php">Mensaje</a></li>
        <li><a href="facturar.php">Facturar</a></li>
        <li><a href="reportefact.php">Reporte Facturas</a></li>
        
     
      <li><a href="https://www.google.com.py/?gws_rd=ssl" target="_blank">Busqueda</a></li>
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="../cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span> Salir</a></li>
    </ul>
  </div>
</nav>

        <body>
        <div class="container">
        <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }
        
    ?>
    
    <center><h1>Bienvenido Usuario</h1></center>
    <center><?php
    //de esta forma concatenamos el dato de ususario registrado para un saludo personalizado
    echo "Hola: " . $_SESSION["usuario"]. "<br><br>";
        
    ?>
        </center>
  

    </div>
        <div class="container">
            <div class="col-sm-12">
                <div class="well" style="margin-top: 15px;">
                    <h1 class="text-center">Carga de Item para Presupuesto</h1>
                    <b><hr></b>
                    <form action="cargapresu.php" method="POST" name="frm">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Descripcion</label>
                                <input type="text" class="form-control text-danger" name="des" id="des" placeholder="Toldo 6 x 9"  required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Cantidad</label>
                                <input type="number" class="form-control" name="can" placeholder="100" id="can"required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Monto</label>
                                <input type="number" class="form-control" name="monto" placeholder="100"  id="monto" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4">Obligatorio</label>
                                <select class="form-control" id="exampleFormControlSelect1" id="obliga"  name="obliga">                               
                                    <option value="1">Si</option>
                                    <option value="0">No</option>                   
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>      
                                 <div class="form-group col-sm-4">
                                        <button type="submit" style="margin-top: 25px" id="enviar" class="btn btn-success btn-lg pull-center ">Enviar</button>
                                    </div>
                                    <div class="form-group col-sm-4">
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
        </body>
</html>