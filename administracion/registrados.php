<?php require_once ('conexion.php');
$conexion=conectarBD();


function getevento() {
    $con = conectarBD();

    $query = 'SELECT * from evento where estado= TRUE';
 

    $resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
}

function getlistatuevento($id_evento){
$con=conectarBD();
$query="Select cedula,nombre,apellido,tipo_pago,monto from persona where evento= ".$id_evento;
$resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
    
}

$id_evento= isset($_POST['id_evento']) ? $_POST['id_evento'] : '' ;
$data['nombre'] = getevento();
if ($id_evento != ''){
    
    $data['evento'] = getlistatuevento($id_evento);
    
    
}else{
    $data['evento'] ='';
    
};

?>


<html lang="en">
<head>
  <title>Registrados</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.css">
  
  <script src="../assets/jquery-3.3.1.min.js"></script>
  <script src="../bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>

 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="inicio.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Presupuesto
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="presupuesto1.php">Crear Presupuesto</a></li>
          <li><a href="updatePresu.php">Actualizar Presupuesto</a></li>
          
        </ul>
      </li>
        <li><a href="itempresu.php">Item de Presupuesto</a></li>
        <li><a href="registrados.php">Registrados</a></li>
        <li><a href="mensaje.php">Mensaje</a></li>
        <li><a href="facturar.php">Facturar</a></li>
        <li><a href="reportefact.php">Reporte Facturas</a></li>
      
     
      <li><a href="https://www.google.com.py/?gws_rd=ssl" target="_blank">Busqueda</a></li>
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="../cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span> Salir</a></li>
    </ul>
  </div>
</nav>
   
     
    
       <div class="container">
            <div class="container">
        <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }
        
    ?>
    
    <center><h1>Bienvenido Usuario</h1></center>
    <center><?php
    //de esta forma concatenamos el dato de ususario registrado para un saludo personalizado
    echo "Hola: " . $_SESSION["usuario"]. "<br><br>";
        
    ?>
        </center>
  

    </div>
       <div class="form-group col-sm-3">
           <form action="registrados.php" method="post">
        <label for=exampleFormControlSelect1" class="h4">Evento</label>
        <select class="form-control" id="exampleFormControlSelect1" id="id_evento"  name="id_evento">
        <?php
        foreach ($data['nombre'] as $d) :
        ?>
            <option class="po" value="<?php echo $d->id_evento; ?>" > <?php echo $d->nombre; ?>  </option>
            <?php endforeach; ?>
              </select>  
                </div>
           <div class="form-group col-sm-4">
           <button type="submit" style="margin-top: 5px" id="listar" class="btn btn-success btn-lg pull-center ">Listar</button>
           </div>
           </form>
            <table class="table table-bordered">
    <thead>
      <tr>
        <th>Cedula</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Forma de Pago</th>
        <th>Monto</th>
      </tr>
    </thead>
    <tbody>
    
        <?php if ($data['evento'] != ''):
     foreach ($data['evento'] as $d):?>
            <tr>
        <td><?php echo $d->cedula; ?></td>
        <td><?php echo $d->nombre; ?></td>
        <td> <?php echo $d->apellido; ?></td>
        <td><?php echo $d->tipo_pago;?></td>
        <td><?php echo $d->monto; ?></td>
      </tr>
      <?php endforeach;?>
        
          <?php else :  ?>
        <tr>
        <td><?php  ?></td>
        <td><?php  ?></td>
        <td> <?php  ?></td>
        <td><?php ?></td>
        <td><?php ?></td>
      </tr>
       <?php endif;  ?>
    </tbody>
  </table>
  <?php if($id_evento != ''):?>
  
  <li><a href="excelregistra.php?idregist=<?php echo $id_evento ?>">Descargar Excel</a></li>
  <?php endif?>

    </div>

</body>

</html>
