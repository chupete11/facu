<?php require_once ('conexion.php');
$conexion=conectarBD();?>

<html lang="en">
<head>
  <title>Inicio</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.css">
  
  <script src="../assets/jquery-3.3.1.min.js"></script>
  <script src="../bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="inicio.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Presupuesto
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="presupuesto1.php">Crear Presupuesto</a></li>
          <li><a href="updatePresu.php">Actualizar Presupuesto</a></li>
          
        </ul>
      </li>
        <li><a href="itempresu.php">Item de Presupuesto</a></li>
        <li><a href="registrados.php">Registrados</a></li>
        <li><a href="mensaje.php">Mensaje</a></li>
        <li><a href="facturar.php">Facturar</a></li>
        <li><a href="reportefact.php">Reporte Facturas</a></li>
        
     
      
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="../cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span> Salir</a></li>
    </ul>
  </div>
</nav>
    <div class="container">
        <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }
        
    ?>
    
    <center><h1>Bienvenido Usuario</h1></center>
    <center><?php
    //de esta forma concatenamos el dato de ususario registrado para un saludo personalizado
    echo "Hola: " . $_SESSION["usuario"]. "<br><br>";
        
    ?>
        </center>
  

    </div>
    
       <div class="container">
            

    </div>

</body>
<footer class="container-fluid text-center">
  <p>Global Running</p>
  <p>Fernando de la Mora, zona Sur</p>
  <p>Telefonos:021555.555 Celular:0981555.555</p>
</footer>
</html>
