<?php
require_once ('conexion.php');
$con= conectarBD();

session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
}


function getdatos(){
    $con= conectarBD();
    $query="SELECT *  from fact";
    
    $resul= pg_query($con, $query);
    $data= pg_fetch_object($resul);
    
    
    return $data;
}

function getfac(){
    $con= conectarBD();
    $query="SELECT count (*) as total  from factura";
    
    $resul= pg_query($con, $query);
    $data1= pg_fetch_object($resul);
    
    
    return $data1->total;
}

function getclien($idCliente){
    $con= conectarBD();
    $query="SELECT * from cliente where id_clie =".$idCliente;
    
    $resul= pg_query($con, $query);
    $data= pg_fetch_object($resul);
    
    
    return $data;
}
function getfact(){
    $con= conectarBD();
    $query="SELECT * from factura";
    
    $resul= pg_query($con, $query);
    $data3= pg_fetch_object($resul);
    
    
    return $data3;
}

function getPresupuesto($idPresu){
  $con= conectarBD();
  $query="select * from control_pres where id_pres = ".$idPresu;
  
  $resul= pg_query($con, $query);
  $result= pg_fetch_object($resul);
  
  
  return $result;
}

function getItemsP($idPre){
  $con= conectarBD();
  $query="select * from presupuesto where id_pres =".$idPre.";";
  
  $resul= pg_query($con, $query);
  $result= pg_fetch_object($resul);
  
  return $result;
}

function getPrsupuestoAll(){
  $con = conectarBD();
  $query = 'SELECT * from get_eventojson;';
  $dataR = pg_query($con, $query);
  $resul = array();
  while ($data = pg_fetch_object($dataR) ){
      $resul[] = $data;
  }
  return $resul;
}

$dataevenlist = getPrsupuestoAll();
$idEvento = isset($_POST['sel1']) ? $_POST['sel1'] : '';

if($idEvento != ''){
$data= getdatos();
$fecha= date('d/m/Y');
$res=1+ $data1=getfac();

$data3= getfact();
$dataPresupuesto = getPresupuesto($idEvento);
$dataItems = json_decode($dataPresupuesto->descripcion);
$data2= getclien($dataItems->idCliente);
}



?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"> 
      <!-- Latest compiled and minified CSS -->
     <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="../assets/jquery-3.3.1.min.js"></script>
    <!-- Latest compiled JavaScript -->
    
    <script src="../bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  </head>
  <style>
  th.mon{
    text-align:right ;
    
  }
  td.mont{
    text-align:right ;
    
  }
  td.mont1{
    text-align:right ;
    
  }

  
  
  </style>


<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="inicio.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
      
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Presupuesto
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="presupuesto1.php">Crear Presupuesto</a></li>
          <li><a href="updatePresu.php">Actualizar Presupuesto</a></li>
          
        </ul>
      </li>
        <li><a href="itempresu.php">Item de Presupuesto</a></li>
        <li><a href="registrados.php">Registrados</a></li>
         <li><a href="mensaje.php">Mensaje</a></li>
        <li><a href="facturar.php">Facturar</a></li>
        <li><a href="reportefact.php" >Reporte Facturas</a></li>
     
      <li><a href="#">Buscar Presupuesto</a></li>
      
    </ul>
      <div class="nav navbar-nav" style="margin-top: 5px">
      <form class="form-inline" action="#" method="post">
      <div class="form-group">
          <select class="form-control input-sm" name="sel1">
            <?php foreach ($dataevenlist as $d) :?>                          
              <option class="po" value="<?php echo $d->id_pres; ?>"  > <?php echo $d->evento; ?>  </option>
            <?php endforeach; ?>
          </select>
    </div>
    <button type="submit" class="btn btn-default btn-sm">Buscar</button>
      </form>
      </div>
    <ul class="nav navbar-nav navbar-right" style="margin: 0px">
    <li><a href="#"><span class="glyphicon glyphicon-user"></span><?php echo ' '.$_SESSION["usuario"] ?></a></li>
        <li><a href="../cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span> Salir</a></li>
    </ul>
  </div>
</nav>         
    <body>
  <?php if($idEvento != ''): ?>    
    <div class="container well" style="margin-top:15px">
        <div class="row">
            <div class="col-lg-12">
                <center>
                <table class="table-bordered">
                <tr>
                    <td width="600" >
                    <center><h5><?php echo $data->empre  ?></h5>
                    <p><?php echo $data->direcc?></p>
                    <p><?php echo $data->cuidad  ?></p>
                    <p>Telefono:<?php echo $data->telel;?> . <?php echo $data->cel;  ?></p></center>
                    </td>
                    <td width="600" >
                    <center>  <p>TimbradoNro:<br><?php echo $data->tim_nro;?></p>
                    <p>Valido Hasta:<br><?php echo $data->valid;?></p>
                    <p>Ruc:<?php echo $data->ruc;?></p>
                    <h5 id="nroFactura" data-nro="<?php echo $res;?>">Factura Nro:<?php echo $res;?></h5></center>
                    </td>
                </tr>
                </table>
            </center>        
                
            </div>
            <div class="col-lg-12">
                <center>
                <table class="table-bordered">
                <tr>
                    <td width="600" >
                    <p style="padding-left: 5px" id="fechaE" data-fecha="<?php echo $fecha;?>">Fecha de Emision:<?php echo $fecha;?></p>
                    </td>
                    <td width="600" style="padding: 3px" >
                    <p style="padding-left: 15px">Condicion de venta:
                <input type="radio" name="forma_pago" id="pago" value="credito"/> <label for="credito">Credito</label>
                <input type="radio" name="forma_pago" id="pago" value="contado" /> <label for="contado">Contado</label>
                    </p>
                    </td>
                </tr>
                </table>
            </center>
            </div>
            <div class="col-lg-12">
            <table class="table-bordered">
                <tr>
                  <td width="200">
                  <div class="form-group" style="padding: 5px">
                  <label for="Ruc">Ruc:</label>
                  <input type="text" class="form-control" id="ruc" placeholder="Ingrese Ruc" name="ruc" value="<?php  echo $data2->cedula_clien;?>">
                  </div>
                  </td>
                  <td width="200">
                  <div class="form-group" style="padding: 5px">
                  <label for="pwd">Razon Social:</label>
                  <input type="text" class="form-control" id="pwd" placeholder="Ingrese Razon Social" name="pwd" value="<?php  echo $data2->nombre_clien;?>">
                  </div>
                  </td>
                  <td width="400">
                  <div class="form-group" style="padding: 5px">
                  <label for="pwd">Direccion:</label>
                  <input type="text" class="form-control" id="direccion" placeholder="Ingrese Direccion" name="direccion" value="">
                </div>
                  </td>
                  </td>
                  <td width="400" style="padding: 5px" colspan="2">
                  <button type="button" id="guardar" class="btn btn-default">Guardar</button>
                  </td>

                  
                  
                </tr>
            </table>      
            </div>
            <div class="col-lg-12">
            <table class="table-bordered">
                <tr>
                    <th  width="400" >Cantidad</th> 
                    <th  width="400" >Descripcion</th>
                    <th  width="400" class="mon" >Monto</th>
                </tr>
                  <?php foreach($dataItems->items as $itm) : 
                    $linea = getItemsP($itm);
                    ?>
                    <tr>
                    <td>1</td>
                    <td><?php echo $linea->descripcion;?></td>
                    <td class="mont"><?php echo number_format($linea->monto,0,' ', '.');?></td>
                    </tr>
                  <?php endforeach;?>
            </table>
            </div>
            <div class="col-lg-12" id="Montos" data-subTotal="<?php echo $dataPresupuesto->total_presu ?>" data-iva = "<?php echo ($dataPresupuesto->total_presu * 0.10) ?>" data-total="<?php echo (($dataPresupuesto->total_presu * 0.10) + $dataPresupuesto->total_presu) ?>" data-idPress="<?php echo $idEvento;?>">
              <table class="table-bordered" >
                <tr>
                  <td width="750"></td>
                  <td width="400" class="mont1">
                    <label >Sub Total:</label><?php echo ' '.number_format($dataPresupuesto->total_presu,0,' ', '.') ?></td>
                </tr>
                <tr>
                  <td width="750"></td>
                  <td  width="400" class="mont1">
                    <label >Iva 10%:</label><?php echo ' '.number_format($dataPresupuesto->total_presu * 0.10,0,' ', '.') ?>
                  </td>
                </tr>
                <tr>
                  <td width="750"></td>
                  <td  width="400" class="mont1">
                  
                  <label >Total:</label><?php echo ' '.number_format(($dataPresupuesto->total_presu * 0.10) + $dataPresupuesto->total_presu,0,' ', '.') ?></td>
                </tr>     
              </table>
            </div>
            <div class="col-lg-12"></div>
            <div class="col-lg-12"></div>
        </div>
    </div>
    <?php endif ?>
    </body>
    <script src="../js/factura.js"></script>
</html>