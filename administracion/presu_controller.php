<?php
require_once ('conexion.php');
session_start();

function getItemsP(){
    $con = conectarBD();
    
    $query = 'select id_pres, INITCAP(descripcion) as "descripcion", obligatorio, total FROM presupuesto ORDER BY id_pres';
    
    $resut = pg_query($con,$query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)){
        $resulta[] = $data;
    }
    return $resulta;
}

function insEvento($data){
    $datos = json_encode($data);
    $con = conectarBD();
    $query = "INSERT INTO control_pres (descripcion, estado, total_presu, fecha_alta, id_cliente)
        VALUES('".$datos."',1,".$data['DescripEvent']['TotalP'].",date(now()),".$data['idCliente']."); select currval(pg_get_serial_sequence('control_pres','id_pres'))";
    $result =  pg_query($con,$query);
    
    $resultado = pg_fetch_object($result);
    return $resultado->currval;
}

function getCliente($filtro){
    $filtro = $filtro == '' || $filtro == ' ' ? '#' : $filtro;
    $con = conectarBD();
    $query = "select 
                id_clie, 
                nombre_clien,
                apellido_clien   
                from cliente where lower(nombre_clien) LIKE '%".$filtro."%'";
    $resul = pg_query($con,$query);
    $resultados = array();
    while ($data = pg_fetch_object($resul)){
        $resultados[] = $data;
    }
    return $resultados;
}

function buidHtml($data){
    $cant = count($data);
        if($cant < 2){
        $html = '';
        foreach ($data as $items){
            $html .= "<input type='text' class='form-control' value='".$items->nombre_clien.' '.$items->apellido_clien."' data-idCliente='".$items->id_clie."' placeholder='nombre evento'>"
                    . "<input type='hidden' class='form-control' value='".$items->id_clie."' id='busqueda1' >";
        }
        return $html;
        } elseif ($cant > 1) {
            $html = '<select class="form-control" id="sel1">';
             foreach ($data as $items){
                $html .= "<option value='".$items->id_clie."' >".$items->nombre_clien.' '.$items->apellido_clien."</option>";
            }
            $html .= '</select>';
        return $html;
    }
}

function getJsonEvento($idEvento){
    $con = conectarBD();
    $query = 'select *,
    (select nombre_clien from cliente where id_clie =  b.id_cliente) as nombreCliente
    from control_pres b where b.id_pres = '.$idEvento;
    $resut = pg_query($con,$query);
    $resulta =  pg_fetch_object($resut);
    /*
    while ($data = pg_fetch_object($resut)){
        $resulta[] = $data;
    }*/
    return $resulta;
}

function updateEvento($data, $idPresu){
    $datos = json_encode($data);
    $con = conectarBD();
    $query = "UPDATE control_pres 
                set descripcion = '".$datos."', estado = 1, total_presu = ".$data['DescripEvent']['TotalP'].", fecha_alta = date(now())
                where id_pres =". $idPresu;
    $result =  pg_query($con,$query);
    
    $resultado = pg_affected_rows($result);
    return $resultado;
}



$tipo = isset($_POST['tipo'])? $_POST['tipo'] : 0 ;

switch ($tipo){
case 1:
    $var2 = isset($_POST['datos'])? $_POST['datos'] : 0 ;
    if($var2){
       $data['idCliente'] =  isset($_POST['idCliente']) ? $_POST['idCliente'] : 0 ;
       $data['km'] = explode('/', substr($var2['listaOpcion'], 0, -1));
       $data['items'] = explode('/', substr($var2['listaOpcion2'], 0, -1));
       $data['DescripEvent'] = array('evento' => $var2['evento'], 
           'fecha' => $var2['fecha'], 
           'lugar' => $var2['lugar'], 
           'cantPersona' => $var2['cantC'],
           'TotalP' => $var2['TotalPagar']);
          $var = insEvento($data); 
    } 
break;
case 2:
    $filtro = isset($_POST['letra'])? $_POST['letra'] : '';
    $dataClientes = getCliente($filtro);
    $clienteHmtl =  buidHtml($dataClientes);
    echo $clienteHmtl;
break;    
case 3:
    $idEvento = $_POST['idEvent'];
    $result = getJsonEvento($idEvento);
    $data['jsonData'] = $result;
    echo json_encode($data);
break;
case 4 :
    $var2 = isset($_POST['datos'])? $_POST['datos'] : 0 ;
    if($var2){
    $idPresu = isset($_POST['idPresupuesto']) ? $_POST['idPresupuesto'] : 0 ;   
    $data['idCliente'] =  isset($_POST['idCliente']) ? $_POST['idCliente'] : 0 ;
    $data['km'] = explode('/', substr($var2['listaOpcion'], 0, -1));
    $data['items'] = explode('/', substr($var2['listaOpcion2'], 0, -1));
    $data['DescripEvent'] = array('evento' => $var2['evento'], 
        'fecha' => $var2['fecha'], 
        'lugar' => $var2['lugar'], 
        'cantPersona' => $var2['cantC'],
        'TotalP' => $var2['TotalPagar']);
        $var['success'] = updateEvento($data,$idPresu);
        echo json_encode($var);
    }
break;
default :
    $data['listaItem'] = getItemsP();
    break;
}



?>
