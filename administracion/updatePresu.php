<?php
require_once ('conexion.php');
$con=conectarBD();
require_once ('presu_controller.php');

function get_Presu(){
$con=conectarBD();

$query="SELECT 
*,  
substr(descripcion, strpos(descripcion, 'evento')+9, (strpos(descripcion, 'fecha')-3)-strpos(descripcion, 'evento')-9)  as PesupuetoName
FROM control_pres where estado=1";
$result=pg_query($con,$query);
$resultado= array();
while ($data=pg_fetch_object($result)){
    $resultado[]=$data;
}
return $resultado;

};

$data1=get_Presu();

?>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <script src="../assets/jquery-3.3.1.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <script src="../bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" href="../css/estilo13.css">
        <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script>
            localStorage.url = 'http://localhost:8080/runningsys/administracion';
        </script>    
    </head>
    <body>
        <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="inicio.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
    
        
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Presupuesto
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="presupuesto1.php">Crear Presupuesto</a></li>
          <li><a href="updatePresu.php">Actualizar Presupuesto</a></li>
          
        </ul>
      </li>
        
        <li><a href="itempresu.php">Item de Presupuesto</a></li>
        <li><a href="registrados.php">Registrados</a></li>
        <li><a href="mensaje.php">Mensaje</a></li>
        <li><a href="facturar.php">Facturar</a></li>
        <li><a href="reportefact.php">Reporte Facturas</a></li>
        
     
      <li><a href="https://www.google.com.py/?gws_rd=ssl" target="_blank">Busqueda</a></li>
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="../cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span> Salir</a></li>
    </ul>
  </div>
</nav>
        <div class="container">
                <div class="col-sm-6 col-sm-offset-3">
    <div class="well" style="margin-top: 10%;">
    <h3 class="text-center">Presupuesto</h3>
        <form role="form" method="post" id="contactForm" data-toggle="validator" class="shake" action="presupuestoUpdate.php">
        <div class="row">
            <div class="form-group col-sm-6">         
                <label for="sel1" class="h4 text-center">Presupuesto:</label>
                <select name="sel1" class="form-control" id="sel1" >
                    <option value="-1">Seleccionar Presupuesto</option>
                    <?php foreach($data1 as $d):?>
                    <option value="<?php echo "$d->id_pres"; ?>"><?php echo "$d->pesupuetoname"; ?></option>
                    <?php endforeach;?>
                </select>
            </div>
            
            <div class="form-group col-sm-6">
                <label for="name" class="h4 text-center">Cliente</label>
                <input type="text" class="form-control" id="cliente" value="0">
                <input type="hidden" class="form-control" id="idClie" value="0">
                <div id="pruebas"></div>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-sm-6">
                <label for="name" class="h4 text-center">Nombre Evento</label>
                <input type="text" class="form-control" id="evento" placeholder="nombre evento"  data-error="NEW ERROR MESSAGE">
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-sm-6">
                <label for="email" class="h4 text-center">Fecha</label>
                <input type="date" class="form-control" id="fecha" placeholder="fecha" >
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-sm-6">
                   <label for="message" class="h4 ">Lugar</label>
                    <textarea id="lugar" class="form-control" rows="1" placeholder="lugar" ></textarea>
                    <div class="help-block with-errors"></div>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-sm-6">
                <label for="email" class="h4">Cantidad Corredor</label>
                <input type="number" class="form-control" value=0 name="cantprecio" id="cantC" placeholder="cant Corredor" >
                <div class="help-block with-errors"></div>
            </div>
        </div>
        
        <div class="form-group">
            
            <label for="name" class="h4 text-center">Distancia</label>
                <div class="row">
                <div class="col-md-12">
                <label class="checkbox-inline"><input type="checkbox" value="5" name="lista">KM 5</label>
                <label class="checkbox-inline"><input type="checkbox" value="7" name="lista">KM 7</label>
                <label class="checkbox-inline"><input type="checkbox" value="10" name="lista">KM 10</label>
                <label class="checkbox-inline"><input type="checkbox" value="15" name="lista">KM 15</label>
                <label class="checkbox-inline"><input type="checkbox" value="21" name="lista">KM 21</label>
                <label class="checkbox-inline"><input type="checkbox" value="42" name="lista">KM 42</label>
                </div>    
                </div> 
        </div>
         <hr>
         <div class="row">
             <?php foreach ($data['listaItem'] as $list): 
                 if($list->id_pres == 1){
                     $costoPersona = $list->total;
                 }
                 ?>
             <input type="hidden" name="valorPersona" value="<?php echo $costoPersona ?>">
             <ul class="list-group" style="margin: 0px" id="listItems">
                 <li class="list-group-item">
                     <input type="checkbox" data-precio="<?php echo $list->total?>" name="lista2" value="<?php echo $list->id_pres; ?>" <?php echo $cke = $list->obligatorio  == 1 ? 'checked disabled' : ''  ;?> >
                     <?php echo ' '.$list->descripcion ?>
                     <span class="badge"><?php echo number_format($list->total, 0,' ', '.')  ?></span></li> 
              </ul>
            <?php endforeach;?>
             <ul class="list-group" style="margin: 0px">
                 <li class="list-group-item" >
                     <strong>Total:</strong> 
                     <span class="badge" id="Total"></span>
                 </li> 
              </ul>
         </div>
            
         <button type="button" style="margin-top: 5px" id="prueba" class="btn btn-info btn-lg pull-right ">Actualizar</button>
         <input type="submit" id="pdf" style="margin-top: 5px; display: none" class="btn btn-warning btn-lg pull-right"  value="PDF"  >
        <div id="msgSubmit" class="h3 text-center hidden"></div>
        <div class="clearfix"></div>
    </form>
    </div>
</div>
        </div>
        <script src="../js/presupuesto.js"></script>
        <script src="../js/udate_presu.js"></script>
        
    </body>
</html>

