<?php require_once ('conexion.php');
$conexion=conectarBD();


function getevento() {
    $con = conectarBD();

    $query = 'SELECT DISTINCT form_pago from factura';
 

    $resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
}

function getlistatuevento($id_fact){
$con=conectarBD();
$query="SELECT * from factura where form_pago= '".$id_fact."'";
$resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
    
}

$id_fact= isset($_POST['id_fact']) ? $_POST['id_fact'] : '' ;
$data['formaP'] = getevento();
if ($id_fact != ''){
    
    $data['id_fact'] = getlistatuevento($id_fact);
    
    
}else{
    $data['id_fact'] = '';
    
};

?>


<html lang="en">
<head>
  <title>Registrados</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.css">
  
  <script src="../assets/jquery-3.3.1.min.js"></script>
  <script src="../bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>

 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="inicio.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Presupuesto
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="presupuesto1.php">Crear Presupuesto</a></li>
          <li><a href="updatePresu.php">Actualizar Presupuesto</a></li>
          
        </ul>
      </li>
        <li><a href="itempresu.php">Item de Presupuesto</a></li>
        <li><a href="registrados.php">Registrados</a></li>
        <li><a href="mensaje.php">Mensaje</a></li>
        <li><a href="facturar.php">Facturar</a></li>
        <li><a href="reportefact.php">Reporte Facturas</a></li>
      
     
      <li><a href="https://www.google.com.py/?gws_rd=ssl" target="_blank">Busqueda</a></li>
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="../cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span> Salir</a></li>
    </ul>
  </div>
</nav>
   
     
    
       <div class="container">
            <div class="container">
        <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }
        
    ?>
    
    <center><h1>Bienvenido Usuario</h1></center>
    <center><?php
    //de esta forma concatenamos el dato de ususario registrado para un saludo personalizado
    echo "Hola: " . $_SESSION["usuario"]. "<br><br>";
        
    ?>
        </center>
  

    </div>
       <div class="form-group col-sm-3">
           <form action="reportefact.php" method="post">
        <label for=exampleFormControlSelect1" class="h4">Tipo Factura</label>
        <select class="form-control" id="exampleFormControlSelect1" id="id_fact"  name="id_fact">
        <?php
        foreach ($data['formaP'] as $d) :
        ?>
            <option class="po" value="<?php echo $d->form_pago; ?>" > <?php echo $d->form_pago; ?>  </option>
            <?php endforeach; ?>
              </select>  
                </div>
           <div class="form-group col-sm-4">
           <button type="submit" style="margin-top: 5px" id="listar" class="btn btn-success btn-lg pull-center ">Listar</button>
           </div>
           </form>
            <table class="table table-bordered">
    <thead>
      <tr>
        <th>Identidicador</th>
        <th>Cantidad</th>
        <th>Descripcion</th>
        <th>Monto</th>
        <th>Iva</th>
        <th>Nur.Factura</th>
        <th>Tipo Pago</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody>
    
        <?php if ($data['id_fact'] != ''):
     foreach ($data['id_fact'] as $d):?>
            <tr>
        <td><?php echo $d->id_fact; ?></td>
        <td><?php echo $d->canti; ?></td>
        <td><?php echo $d->descripcion; ?></td>
        <td> <?php echo $d->monto; ?></td>
        <td><?php echo $d->iva;?></td>
        <td><?php echo $d->num_fact; ?></td>
        <td><?php echo $d->form_pago; ?></td>
        <td><?php echo $d->total; ?></td>
      </tr>
      <?php endforeach;?>
        
          <?php else :  ?>
        <tr>
        <td><?php  ?></td>
        <td><?php  ?></td>
        <td> <?php  ?></td>
        <td><?php ?></td>
        <td><?php ?></td>
        <td> <?php  ?></td>
        <td><?php ?></td>
        <td><?php ?></td>
      </tr>
       <?php endif;  ?>
    </tbody>
  </table>
  <?php if($id_fact != ''):?>
  
  <li><a href="excelfactura.php?idCredito=<?php echo $id_fact ?>">Descargar Excel</a></li>
  <?php endif?>
    </div>

</body>

</html>
