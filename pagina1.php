<?php
require_once ('conexion.php');
$conexion = conectarBD();
$evento = isset($_GET["e"]) ? $_GET["e"] :'';

function getTipoPago() {
    $conexion = conectarBD();
    $query = "select * from tipo_pago where estado = 1";
    $resut = pg_query($conexion, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }
    // $data = pg_fetch_object($resut);
    return $resulta;
}

$data['tipoPago'] = getTipoPago();


// var_dump($data['tipoPago']); 
?>

<html>
    <head>
        <title>Registro1</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/estilo7.css">

    </head>
    <div id="meta">
        <img src="imagen/cabecera1.png" width="1100" height="120">
    </div>
    <div class="nav">
        <ul>

            <a href="index.php"><img src="iconos/Inicio.png"></a>
            <a href="index8.php"><img src="iconos/Eventos.png"></a>
            <a href="index7.php"><img src="iconos/Calendario.png"></a>
            <a href="index5.php"><img src="iconos/Nosotros.png"></a>
            <!--<a href="index3.html"><img src="iconos/Ubicacion.png"></a>-->
            <a href="https://www.google.com.py/?gws_rd=ssl" target="_blank"><img src="iconos/busqueda.png"></a>
            <a href="login.php"><img src="iconos/login.png"></a>
        </ul>

    </div>
    <body>


    <CENTER> <section id="este" >
            <h1 >Benvenido</h1>
            <form action="ficha.php" method="POST" name="frm">
                <table>
                    <td>
                    <center><h4>Ingrese Numero de Cedula:</h4>
                        <input type="text" name="cedula" id="cedula" required>
                        <input type="hidden" name="evento" value="<?php echo $evento; ?>">
                    </center>
                    </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea name="termino" rows="9" readonly id="termino" style="max-width:90%;width:750px;font-family:Arial, Helvetica, sans-serif; font-size:10px">Los datos personales que fueron proveidos para efectivizar esta inscripción al evento de son de mi total responsabilidad.
Leí y estoy de acuerdo con el reglamento del evento, disponible a través de la página web .
Participo del evento por voluntad libre y espontánea, exonerando de cualquier responsabilidad a los Organizadores, Patrocinadores y Realizadores, en nombre mío y de mis sucesores.
Estoy conciente de mi estado de salud y de estar capacitado para la participación, gozando de una perfecta salud y de haber entrenado adecuadamente para este evento.
Asumo la responsabilidad de indemnizar por daños personales o materiales causados por mi persona durante mi participación en este evento.
Por este instrumento, cedo todos los derechos de utilización de mi imagen, renunciando al recibimiento de cualquier remuneración por los materiales de divulgación, campañas, informaciones, transmición de tv, clips, representaciones, materiales periodísticos, promociones comerciales, licenciamentos y fotos, en cualquier momento, por cualquier medio actualmente disponibles o que vengan a ser implementados para este evento.
Estoy conciente que, al registrarme o inscribirme, estoy incluyendome automáticamente en la base de datos del Paraguay Marathon Club, autorizando a este, desde ya, a enviar a la dirección electrónica o física registrada, cualquier tipo de correspondencia electrónica o física para participar de cualquier promoción o acción promocional y programas de incentivo o comerciales, desarrollada la por la misma o por sus asociados.
Asumo todos los gastos de viaje, hospedaje, alimentación, traslados, seguros, asistencia médica y cualquier otro gasto necesario, proveniente de mi participación en este evento; antes, durante o después del mismo.
Acepto no portar, ni tampoco utilizar dentro de las áreas del evento, incluyendo el trayecto y la zona de entrega de kits, o cualquier area de visibilidad del evento expuesta al público, o medios de divulgación y promoción, ningún material político, promocional o publicitario, ni letreros que puedan ser vistos por los demas sin autorización por escrito de la organización; y también, ningún tipo de material u objeto que ponga en riesgo la seguridad del evento, participantes y /o de las personas presentes, aceptando ser retirado por la organización o autoridades de las áreas descriptas arriba.
En caso de participación en este evento, representando equipos de participantes o prestadores de servicios y/o cualquier medio vehículo, declaro tener pleno conocimento y que acepto el reglamento del evento, y asi también respetar las áreas de la organización destinadas para las mismas, evitando las estructuras de apoyo para equipos montadas en locales inadecuados, o que interfieran con el espacio del evento y también los locales sin autorización por escrito de la organización, pudiendo ser retirados en cualquier momento.
Independientemente de estar presente o no en el acto de la inscripción, asi como la retirada de mi kit de participación, que fuera debidamente efectuada por mi persona, o por intermedio de mi entrenador; declaro estar conciente de todo el reglamento de la prueba, y asi también de mis derechos y obligaciones dentro del evento, habiendo tomado pleno conocimiento de las normas y reglamentos de la prueba.
Estoy conciente que todos los servicios estarán disponíbles solamente despues del pago de la inscripción, debiendo acceder al sitio web del evento para verificar posibles modificaciones de este reglamento, notícias e informaciones sobre la disponibilidad de los servicios y/o cortesías para mi participación en el evento.
El participante queda conciente de que la responsabilidad del uso del chip e instalación en su cuerpo / vestuario y de su responsabilidad, asi como la pasada por la alfombra de captación de datos instalada, pues se tratan de equipamientos eletrónicos y los mismos pueden sufrir algún tipo de interferência y/o perdida de las informaciones, quedando excenta la organización y los Realizadores de la provisión de los mismos.
La Talla de las Remeras oficiales se entregan de acuerdo a la disponibilidad de stock de las mismas.
LA ORGANIZACIÓN SE RESERVA EL DERECHO DE ADMISIÓN AL EVENTO.</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                    <center>
                        <label> 
                            <input type="checkbox" name="acepto" id="acepto" required/>Acepto los T&eacute;rminos de Reponsabilidad del evento</label></center>
                    </p>

                    </td>
                    </tr>
                    <tr> 
                        <td>

                    <CENTER>   <h2>Forma de Pago</h2><br>
                        <select id="pago" name="pago"> 
                            <option value="-1" label="" >Seleccione Tipo de Pago</option>
                            <?php foreach ($data['tipoPago'] as $tip): ?>
                                <option value="<?php echo $tip->tipo_pago; ?>"><?php echo $tip->tipo_pago;?></option>
                            <?php endforeach; ?>
                        </select>
                    </CENTER>

                    </td>   
                    </tr>

                </table><br>
                <div>
                    <tr> 
                        <td><input type="submit" name="Siguiente"></input>
                        </td>
                    </tr>
                </div>
            </form>
        </section>
    </body>
</html>
