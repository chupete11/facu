<?php
require_once ('conexion.php');
// $con= conectarBD();
$id = isset($_POST["id_evento"]) ? $_POST['id_evento'] : '';

function getevento() {
    $con = conectarBD();

    $query = 'select * from evento where estado= true ';

    $resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
}
$data['id_evento'] = getevento();

function  getTipo($id){
    $New = (int) $id;
    $con= conectarBD();
    $query = "select 
                    count(p.*) as cantidad,
                    CASE p.sexo WHEN 'F' THEN 'Femenino'
                                ELSE 'Masculino'
                           END as sexo
                    from persona p
                    where p.evento = ".$New."GROUP BY p.sexo";
    $resul= pg_query($con, $query);
    $resultado = array();
    while ($data = pg_fetch_object($resul)){
       $resultado[] =  $data;
    }
   
    return json_encode($resultado) ;
}
function  getPago($id){
    $New = (int) $id;
    $con= conectarBD();
    $query = "select 
                    CASE p.tipo_pago WHEN 'Efectivo' THEN 'Efectivo'
                                WHEN 'Giro Tigo' THEN 'Giro Tigo'
                                WHEN 'Deposito Banco' THEN 'Deposito Banco'
                                ELSE 'Tarjeta'
                                END as label,
                    count(p.*) as value
                    from persona p
                    where p.evento = ".$New."
                    GROUP BY p.tipo_pago";
    $resul= pg_query($con, $query);
    $resultado = array();
    while ($data = pg_fetch_object($resul)){
       $resultado[] =  $data;
    }
   
    return json_encode($resultado) ;
}


function getEventoGrafico(){
    $con= conectarBD();
    $query = "select 
                    e.nombre as y,
                    count(*) as a
                    FROM persona p
                    join evento e
                    on e.id_evento = p.evento
                    where e.estado = TRUE
                    GROUP BY p.evento, e.nombre, e.id_evento";
    $resul= pg_query($con, $query);
    $resultado = array();
    while ($data = pg_fetch_object($resul)){
       $resultado[] =  $data;
    }
    return json_encode($resultado) ;
}

$result = getTipo($id);
$result1= getPago($id);
$resulGraficoEvento = getEventoGrafico();


?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="graficos/morris.js/morris.css">
    <script src="assets/jquery-3.3.1.min.js"></script>
    <script src="graficos/raphael/raphael.min.js"></script>
    <script src="graficos/morris.js/morris.min.js"></script>
    
    <title>Graficos</title>
  </head>
  <body>
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="mantenimiento.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        <li ><a class="nav-brand active" href="registrados.php">Lista de Registrados</a></li>
      <li><a href="busqueda1.php">Buscar por Cedula</a></li>
      <li><a href="actualizar.php">Actualizar Registros</a></li>
      
      <li><a href="borrar.php">Borrar Registros</a></li>
      <li><a href="crearevento.php">Cargar Evento</a></li>
      <li><a href="remeras.php">Talla Remera</a></li>
      <li><a href="tallas.php">Saldo Talla</a></li>
     </li>
      <ul class="nav navbar-nav">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Control Evento
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="controlEvento.php">Estado Evento</a></li>
          <li><a href="grafico.php">Grafico</a></li>
          <li><a href="regisUsuario.php">Registrar Usuario</a></li>
        </ul>

      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span>Salir</a></li>
    </ul>
  </div>
</nav>
  <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }    
    ?>
      
      <div class="container">
          <form action="grafico.php" method="POST" name="frm">
          <h1>Grafica</h1>
         <div class="col-sm-4">
            <label for=exampleFormControlSelect1" class="h4">Selecione Evento</label>
            <select class="form-control" id="exampleFormControlSelect1" id="id_evento"  name="id_evento">
                <option class="po" value="-1">Seleccione un evento</option>
           <?php foreach ($data['id_evento'] as $d): ?>
                <option class="po" value="<?php echo $d->id_evento; ?>"  > <?php echo $d->nombre; ?>  </option>
            <?php endforeach; ?>
             </select> 
         </div>
          <div class="form-group col-sm-4">
                <button type="submit" style="margin-top: 30px" id="enviar" class="btn btn-success btn-lg pull-center ">Enviar</button>
            </div>
          <div class="col-sm-12">
         </div>
           <br>
          <div class="row">
              <div class="col-md-12">
                  <h1>Grafica por Evento </h1>
                  <hr>
                  <div id="myfirstchart" data-evento='<?php echo $resulGraficoEvento ?>'></div>
              </div>
              
              <div class="col-md-6">
                  <h1>Grafica por Sexo</h1>
                  <hr>
                  <div id="prueba" data-tipo='<?php echo $result; ?>'>
                  </div>        
              </div>
            <div class="col-md-6">
                  <h1>Grafica Tipo de Pago</h1>
                  <hr>
                  <div id="prueba2" data-pago='<?php echo $result1; ?>'>
                      
                  </div>
              </div>
          </div>
          </form>
 
        </div>
  </body>
  <script src="js/grafico.js"></script>
</html>