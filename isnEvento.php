<?php
require_once ('conexion.php');

function insEvent($data){
    $data['categoria'] = parseoKM($data['categoria']);
    $str = implode("', '", $data);

    $con = conectarBD();
    $query = "INSERT INTO evento (nombre, cantidad_parti, estado, imag, fecha_even, categoria, lugareven, descripcion_even, monto, id_cliente)VALUES('".$str."');";
    $resut = pg_query($con, $query);
    $query2 = 'select id_evento from evento order by id_evento desc limit 1';
    $result2 = pg_query($con, $query2);
    $idInsert = pg_fetch_object($result2);
    pg_close($con);
    return $idInsert;

}


function insCategoria($data){
    $str = implode("', '", $data);
    $con = conectarBD();
    $query = "INSERT INTO categoria (categoria, id_evento)VALUES('".$str."')";
    $resut = pg_query($con, $query);
    pg_close($con);
}

function setPost(){
    foreach($_POST as  $nombre=>$valor){
        $data[$nombre] = $valor;
    }
    return $data;
}

function loadImagen(){
    $archivo = $_FILES["archivo"];
    $resultado = move_uploaded_file($archivo["tmp_name"], 'imagen/'.$archivo["name"]);
    return $resultado;
}

function parseoKM($datakm){
    $resul = explode(',', $datakm);
    $cant = count($resul);
    for($i =0; $cant >$i; $i++){
        $resul[$i] = $resul[$i].' km';
    }
    return implode(', ', $resul);
}

loadImagen();
$datosEvento = setPost();

if(is_array($datosEvento)){
   $idEvento =  insEvent($datosEvento)->id_evento;

    if(isset($idEvento) && $idEvento > 0){
         $categoria = explode(',' , $datosEvento['categoria']); 
         
         for($i = 0; count($categoria) > $i; $i++){
            $dataCategoria['categoria'] = trim($categoria[$i]).' kM';
            $dataCategoria['idEvento'] = $idEvento;
            insCategoria($dataCategoria);
         }
         $data['success'] = 'Evento guardado Correctamente';
         $data['Error'] = false;
         echo json_encode($data);
    }
}else{
    $data['success'] = 'Evento no guardado';
         $data['Error'] = true;
    echo json_encode($data);     
}

?>