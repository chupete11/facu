<?php
require_once ('conexion.php');

function getEventos() {

    $conexion = conectarBD();

    $query = "Select * from evento where estado=true";
    $result = pg_query($conexion, $query);


    return $result;
}


$eventos = getEventos();
?>
<html lang="en">
<head>
  <title>Eventos</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/estilo10.css"></link>

  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <script src="assets/jquery-3.3.1.min.js"></script>
  
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
 <div class="container-fluid">
    <nav class="navbar navbar-inverse">
  
    <div class="navbar-header">
        <a class="navbar-brand" href="index1.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        <li class="active"><a href="index1.php">Inicio</a></li>
      <li><a href="nosotros.php">Nosotros</a></li>
      <li><a href="evento2.php">Evento</a></li>
      
      <li><a href="calendario1.php">Calendario</a></li>
       <li><a href="descarga.php" target="_blank">Descargar Termino</a></li>
      <li><a href="https://www.google.com.py/?gws_rd=ssl" target="_blank">Busqueda</a></li>
    </ul>
        
        <ul class="nav navbar-nav navbar-right">
<!--      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>-->
            <li><a href="login1.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
        </nav>
  </div>
<body>


  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
        <p><img src="imagen/fondo34.jpg" width="200"></p>
        <p><img src="imagen/imagen3.png" width="200"></p>
        <p><img src="imagen/motivacion2.jpg" width="200"></p>
    </div>
    <div class="col-sm-8 text-left"> 
        <center><h1>Bienvenido</h1></center>
      <div align="center" style="max-width: 100%">
            <table border="0" align="center" cellpadding="0" cellspacing="1" style="width: 700px; max-width: 100%">
                <?PHP while ($filas = pg_fetch_array($eventos)) :?>
                <tr bgcolor="#E1ECF2">
                    <td width="100">
                        <a href="pagina4.php?e=<?PHP echo $filas["id_evento"] ?>"><img src="imagen/<?php echo $filas["imag"]; ?>" 
                                                                alt="Corrida por la Educación - DEQUENI 2018" width="100" height="75" border="0" style="border-width:1px; border-color:#000000"></a>
                    </td>
                    <td width="6000" valign="middle" bgcolor="#F1F7FA" style="padding-left: 10px; padding-top:3px">
                        <p><span class="let18"><a href="pagina4.php?e=<?PHP echo $filas["id_evento"];?>"><?PHP echo $filas["descripcion_even"].' - '.$filas["nombre"]; ?></a></span><br>
                                <strong>Distancia:</strong><?PHP echo $filas["categoria"].' '; ?><strong>Fecha</strong><?PHP echo $filas["fecha_even"].' '; ?><strong>Lugar</strong>: Costanera de Asunción</p><strong>Monto:</strong><?PHP echo $filas["monto"]; ?>
                    </td>
                </tr>
                <?PHP endwhile;?>
            </table>
        </div>

        </div>

      <div class="col-sm-2 sidenav">
          <p><img src="imagen/fondo2.jpg" width="200"></p>
        <p><img src="imagen/fondo5.jpg" width="200"></p>
        <p><img src="imagen/corredores-1.jpg" width="200"></p>
    </div>
      
    </div>
   
    
  </div>

</body>
<footer class="container-fluid text-center">
  <p>Global Running</p>
  <p>Fernando de la Mora, zona Sur</p>
  <p>Telefonos:021555.555 Celular:0981555.555</p>
</footer>


</html>