<?php require_once ('conexion.php');
$conexion=conectarBD();


function getevento() {
    $con = conectarBD();

    $query = 'SELECT * from evento where estado= TRUE';
 

    $resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
}

function getlistatalla($id_evento){
$con=conectarBD();
$query="SELECT * from tallas1 WHERE id_evento=".$id_evento;
$resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
    
}

$id_evento= isset($_POST['id_evento']) ? $_POST['id_evento'] : '' ;
$data['nombre'] = getevento();
if ($id_evento != ''){
    
    $data['evento'] = getlistatalla($id_evento);
    
    
}else{
    $data['evento'] ='';
    
};

?>


<html lang="en">
<head>
  <title>Tallas</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.css">
  
  <script src="assets/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  

</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="mantenimiento.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        <li ><a class="nav-brand active" href="registrados.php">Lista de Registrados</a></li>
      <li><a href="busqueda1.php">Buscar por Cedula</a></li>
      <li><a href="actualizar.php">Actualizar Registros</a></li>
      
      <li><a href="borrar.php">Borrar Registros</a></li>
      <li><a href="crearevento.php">Cargar Evento</a></li>
      <li><a href="remeras.php">Talla Remera</a></li>
      <li><a href="tallas.php">Saldo Talla</a></li>
     </li>
      <ul class="nav navbar-nav">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Control Evento
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="controlEvento.php">Estado Evento</a></li>
          <li><a href="grafico.php">Grafico</a></li>
          <li><a href="regisUsuario.php">Registrar Usuario</a></li>
        </ul>

      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span>Salir</a></li>
    </ul>
  </div>
</nav>
   
     
    
       <div class="container">
       <div class="form-group col-sm-3">
           <form action="tallas.php" method="post">
        <label for=exampleFormControlSelect1" class="h4">Evento</label>
        <select class="form-control" id="exampleFormControlSelect1" id="id_evento"  name="id_evento">
        <?php
        foreach ($data['nombre'] as $d) :
        ?>
            <option class="po" value="<?php echo $d->id_evento; ?>" > <?php echo $d->nombre; ?>  </option>
            <?php endforeach; ?>
              </select>  
                </div>
           <div class="form-group col-sm-4">
           <button type="submit" style="margin-top: 5px" id="listar" class="btn btn-success btn-lg pull-center ">Listar</button>
           </div>
           </form>
            <table class="table table-bordered">
    <thead>
      <tr>
        <th>Descripcion</th>
        <th>Cantidad</th>
        <th>Saldo</th>
        </tr>
    </thead>
    <tbody>
    
        <?php if ($data['evento'] != ''):
     foreach ($data['evento'] as $d):?>
            <tr>
        <td><?php echo $d->tallas; ?></td>
        <td><?php echo $d->cantidad; ?></td>
        <td bgcolor="<?php if($d->saldo<=0) echo 'red';else ?>" > <?php echo $d->saldo; ?></td>
       
      </tr>
      <?php endforeach;?>
        
          <?php else :  ?>
        <tr>
        <td><?php  ?></td>
        <td><?php  ?></td>
        <td> <?php  ?></td>
       
      </tr>
       <?php endif;  ?>
    </tbody>
  </table>

    </div>

</body>

</html>
