<?php //
require_once ('conexion.php');

$evento = $_GET["e"] != null  ? $_GET['e'] :'';  


function getkm($evento){
    $con = conectarBD();
    
    $query = 'select * from categoria where id_evento = '.$evento;
    
    $resut = pg_query($con,$query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)){
        $resulta[] = $data;
    }
    
    return $resulta;
    
}

function getmonto($id_evento){
    $conexion = conectarBD();
    
    $query = "select monto from evento where id_evento=".$id_evento;    
    $resut = pg_query($conexion,$query);
    $data = pg_fetch_object($resut);
    return $data;
}
function getpais($evento){
    $con = conectarBD();
    
    $query = 'select descripcion from paises ';
    
    $resut = pg_query($con,$query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)){
        $resulta[] = $data;
    }
    
    return $resulta;
}
function gettalla($evento){
    $con = conectarBD();
    
    $query ="select tallas from tallas1 where id_evento=".$evento;
    
    $resut = pg_query($con,$query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)){
        $resulta[] = $data;
    }
    
    return $resulta;
}
$data['km'] = getkm($evento);
$data['monto']= getmonto($evento);
$data['pais']= getpais($evento);
$data['tallas']= gettalla($evento);

// var_dump($data['monto']);
//var_dump($data['pais']);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="css/estilo1.css"></link>
        <title>Registro</title>
    </head>
    <body>  
        <div id="meta">
            <img src="imagen/cabecera.png" width="100%" height="120">
        </div>
        <div class="nav">
            <ul>

                <a href="index.php"><img src="iconos/Inicio.png"></a>
                <a href="index8.php"><img src="iconos/Registro.png"></a>
                <a href="index7.php"><img src="iconos/Calendario.png"></a>
                <a href="index5.php"><img src="iconos/Nosotros.png"></a>
                <!--<a href="index3.html"><img src="iconos/Ubicacion.png"></a>-->

                <a href="https://www.google.com.py/?gws_rd=ssl" target="_blank"><img src="iconos/busqueda.png"></a>
                <a href="login.php"><img src="iconos/login.png"></a>
            </ul>

        </div>




        <div class="aca">
            <h1>REGISTRO DE CORREDORES</h1>

            <form action="procesar1.php" method="POST" name="frm" />
            <table class="mi">



                <tr>
                    <td width=5%>CedulaNº:</td>
                    <td><input type="text" value="<?php ?>"></input>
                    </td>
                </tr>
                 <tr>
                  <td width=5%>Disctancia</td>

                    <td>
                        <select id="categoria" class="dis" name="dis">
                             <?php foreach ($data['km'] as $d) :?>
                               <option value="<?php echo $d->id_cate ;?>"> <?php echo $d->categoria; ?> </option>
                             <?php endforeach; ?>
                        </select>        
                
                     </td>
                </tr>
                <tr>
                    <td width=2%>Nombre:</td>
                    <td><input type="text" name="nombre" pattern="^[a-zA-Z0-9 ./]*$" required/></input>
                    </td>
                </tr>
                <tr>
                    <td windth=2%>Apellido:</td>
                    <td><input type="text" name="apellido" maxlength="30" pattern="^[a-zA-Z0-9 ./]*$" required/></input>
                    </td>
                </tr>
                <tr>
                    <td windth=2%>FechaNac:</td>
                    <td><input type="date"  name="fecha_nac" format="AAAA-MM-DD" pattern="^[0-9]+[0-9-]+[0-9-]"required/></input>
                    </td>
                </tr>
                <tr>
                    <td windth=2%>Correo:</td>
                    <td><input type="email" title="ejemplo@gmail.com" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" name="email" id="email" required/></input>
                    </td>

                </tr>
                <tr>
                    <td windth=2%>Celular:</td>
                    <td><input type="text" name="celular" pattern="^[+0-9]*$" required/></input>
                        </td>
                </tr>
                    <tr>
                    <td windth=2%>Telefono:</td>
                    <td><input type="text" name="telefono" pattern="^[+0-9]*$" required/></input>
                    </td>
                </tr>
                <tr>
                    <td windth=2%>Sexo:
                        <label>
                            <td><input type="radio" name="sexo"  value="F" required/>F</input>
                        </label>
                        <label>
                            <input type="radio" name="sexo" value="M" >M</input></td>
                    </label>
                    </td>
                </tr>
                <tr>
                    <td width=2%>Direccion:</td>
                    <td><input type="text" name="direccion" pattern="^[a-zA-Z0-9./]*$" required/></input>
                    </td>
                </tr>
                <tr>
                    <td width=2%>Cuidad:</td>
                    <td><input type="text" name="cuidad" pattern="^[a-zA-Z0-9 ./]*$" required/></input>
                    </td>
                </tr>
                <tr>
                    <td class="p">Pais</td>

                    <td>
                        <select id="pais"  name="pais">
                             <?php foreach ($data['pais'] as $d) :?>
                            <option class="po" value="<?php echo $d->descripcion;?>"> <?php echo $d->descripcion; ?> </option>
                             <?php endforeach; ?>
                        </select>        
                
                     </td>
                </tr>
                <tr>
                    <td class="p">Talla</td>

                    <td>
                        <select id="tallas"  name="tallas">
                             <?php foreach ($data['tallas'] as $d) :?>
                            <option class="po" value="<?php echo $d->tallas;?>"> <?php echo $d->tallas; ?> </option>
                             <?php endforeach; ?>
                        </select>        
                
                     </td>
                </tr>
                
                
               
            
                <tr>
                    <td width=2%>Monto:</td>
                    <td>
                        <input type="text"  name="monto" value="<?php echo $data['monto']->monto;?>" disabled />
                    </td>
                </tr>
               <tr>
                        <td><input type="submit" name="Enviar"></input>
                   </td>
              </tr>

              </table>

              </form>
           <br>

      </div>





 </body>
</html>


