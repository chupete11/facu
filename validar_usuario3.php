<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Proyecto final</title>
</head>

<body>
    
<?php

    try{
    // nos conectamos a la base de datos por medio de la libreria PDO
    $base=new PDO("mysql:host=localhost; dbname=usuario", "root","");
    $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $sql="Select * from usuario where id_usuario= :login AND PASSWORD= :password";
    //ejecutamos la consulta 
        $resultado=$base->prepare($sql);
    //htmlentities nos permite eliminar la inyeccion SQL
    $login=htmlentities(addslashes($_POST['login']));
    $password=htmlentities(addslashes($_POST['password']));
    //binvalue nor permite comparar los datos que enviamos con los datos de la base de datos 
    $resultado->bindValue(":login", $login);
    $resultado->bindValue(":password", $password);
    //ejecutamos 
    $resultado->execute();
    
    $numero_registro=$resultado->rowCount();
    
    if($numero_registro !=0){
         // de esta manera iniciamos la seccion para el ususario registrado  
       session_start(); 
        // aqui almacenamos los datos del usuario registrado
        $_SESSION["usuario"]=$_POST["login"];
            header("location:usuario_registro1.php");
        
    }else{
        // con la etiqueta header le direccionamis siempre a la pagina de login
        header("location:login.php");
    }
    
    
    
    
}catch(exception $e){
    
    die("Error: ". $e->getMessage());
}

    
    
?>
    
    
    
</body>
</html>