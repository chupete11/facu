<?php require_once ('conexion.php');
$conexion=conectarBD();
$msg = isset($_GET['msg']) ? $_GET['msg']: '';

function getusuario() {
    $conexion = conectarBD();

    $query = "select count(*) as cantidad from usuario" ;
    $resut = pg_query($conexion, $query);
    $data = pg_fetch_object($resut);
   
    return $data->cantidad;
}
$ress=1+ $data['id_usuario']=getusuario();
?>


<html lang="en">
<head>
  <title>Registro_Usuario</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <script src="assets/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <script>
   var msg = <?php echo '"'.$msg.'"' ?>;
   if(msg != ''){
        alert(msg);
   }
  </script>

</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="mantenimiento.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        <li ><a class="nav-brand active" href="registrados.php">Lista de Registrados</a></li>
      <li><a href="busqueda1.php">Buscar por Cedula</a></li>
      <li><a href="actualizar.php">Actualizar Registros</a></li>
      
      <li><a href="borrar.php">Borrar Registros</a></li>
      <li><a href="crearevento.php">Cargar Evento</a></li>
      <li><a href="remeras.php">Talla Remera</a></li>
      <li><a href="tallas.php">Saldo Talla</a></li>
     </li>
      <ul class="nav navbar-nav">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Control Evento
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="controlEvento.php">Estado Evento</a></li>
          <li><a href="grafico.php">Grafico</a></li>
          <li><a href="regisUsuario.php">Registrar Usuario</a></li>
        </ul>

      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span>Salir</a></li>
    </ul>
  </div>
</nav>
    <div class="container">
        <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }
        
    ?>
    
    <center><h1>Bienvenido Usuario</h1></center>
    <center><?php
    //de esta forma concatenamos el dato de ususario registrado para un saludo personalizado
    echo "Hola: " . $_SESSION["usuario"]. "<br><br>";
        
    ?>
        </center>
  
        <div class="container">
            <div class="col-sm-12">
                <div class="well" style="margin-top: 15px;">
                    <h1 class="text-center">Registro de Usuario</h1>
                    <b><hr></b>
                    <form action="procesarUsuario.php" method="POST" name="frm">
                    
                        <div class="row">
                        <div class="form-group col-sm-0">
                                
                                <input type="hidden"  name="usuario" value="<?php echo $ress; ?>"/>
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Nombre</label>
                                <input type="text" class="form-control text-danger" name="nombre"  placeholder="Nombre" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Contraseña</label>
                                <input type="password" class="form-control" name="contra" placeholder="Contraseña" required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 ">Email</label>
                                <input id="correo" class="form-control" name="Correo" rows="1"  placeholder="Correo"  required>
                                <div class="help-block with-errors"></div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4">Tipo Usuario</label>
                                <select class="form-control" id="exampleFormControlSelect1" id="usuario"  name="usuario">                               
                                    <option value="admin">Admin</option>
                                    <option value="secretaria">Secretaria</option>
                                    <option value="administracio">Contable</option>
                                    <option value="usuario">Usuario</option>                   
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                                    <div class="form-group col-sm-4">
                                        <button type="submit" style="margin-top: 30px" id="enviar" class="btn btn-success btn-lg pull-center ">Enviar</button>
                                    </div>
                                    <div class="form-group col-sm-4">
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
    </div>


</body>

</html>
