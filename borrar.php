<?php require_once ('conexion.php');
$conexion=conectarBD();?>

<html lang="en">
<head>
  <title>Lista de Registrados</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <script src="assets/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
 <style>
      #izq{
          
          height: auto;
          width: auto;
          border: solid 1px;
          
      }
     
      th{
          background-color: #cccccc;
          text-align: center;
      }
      td{
          
          height: 40px;
          width: 100px;
      }
      
      
  </style>
</head>
<body>
     <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login.php");
    }
    ?>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="mantenimiento.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        <li ><a class="nav-brand active" href="registrados.php">Lista de Registrados</a></li>
      <li><a href="busqueda1.php">Buscar por Cedula</a></li>
      <li><a href="actualizar.php">Actualizar Registros</a></li>
      
      <li><a href="borrar.php">Borrar Registros</a></li>
      <li><a href="crearevento.php">Cargar Evento</a></li>
      <li><a href="remeras.php">Talla Remera</a></li>
      <li><a href="tallas.php">Saldo Talla</a></li>
     </li>
      <ul class="nav navbar-nav">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Control Evento
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="controlEvento.php">Estado Evento</a></li>
          <li><a href="grafico.php">Grafico</a></li>
          <li><a href="regisUsuario.php">Registrar Usuario</a></li>
        </ul>

      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span>Salir</a></li>
    </ul>
  </div>
</nav>
    
<center><div class="container">
         <center><h1>Bienvenido Usuario</h1></center>
    <center><?php
    //de esta forma concatenamos el dato de ususario registrado para un saludo personalizado
    echo "Hola: " . $_SESSION["usuario"]. "<br><br>";
        
    ?>
        </center>
    
         <center><div class="container" id="izq">
        <h4>Borra Registro</h4>
        
    <form action="borrar_registro1.php" method="post" name="frm"/>
   
     
        <tr><td width=5% >Ingrese Número de Registro:</td>
        <td><input type="text" name="nro" required></input></td>
        </tr>
        
        
                
           
       <td><input type="submit" name="Borrar"></input></td></tr>
      

        
 
    
    
        
    


    <center><h4>Listado General de Registrados</h4></center>
    <?php
    
    $query="select *  from persona order by id_pers";
    $resultado=pg_query($conexion,$query) or die("Error en la consulta");
    $nr=pg_num_rows($resultado);
    if($nr>0){
        echo"<table border=1 bgcolor=azure >
        <tr><th>Codigo</th><th>Cedula</th><th>Nombre</th><th>Apellido</th><th>FechaNac</th><th>Correo</th><th>Genero</th><th>Celular</th></tr>";
        while ($filas = pg_fetch_array ($resultado)){
        echo "<td>".$filas["id_pers"]."</td>";
        echo "<td>".$filas["cedula"]."</td>";
        echo "<td>".$filas["nombre"]."</td>";
        echo "<td>".$filas["apellido"]."</td>";
        echo "<td>".$filas["fecha_nac"]."</td>";
        echo "<td>".$filas["email"]."</td>";
        echo "<td>".$filas["sexo"]."</td>";
        echo "<td>".$filas["celular"]."</td></tr>";
        }echo "</table>";
        
    }else{
        
        echo"no hay datos";
    }
    ?>
    </div></center>
    
    
    
    </div></center>
    
    
    

   
</body>

</html>
