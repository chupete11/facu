<?php

?>

<html lang="en">
<head>
  <title>Inicio</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <script src="js/carrucel.js"></script>
  <link rel="stylesheet" href="css/estilo16.css"></link>
  <script src="assets/jquery-3.3.1.min.js"></script>
  
  <script src="bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="index1.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index1.php">Inicio</a></li>
      <li><a href="nosotros.php">Nosotros</a></li>
      <li><a href="evento2.php">Evento</a></li>
      <li><a href="calendario1.php">Calendario</a></li>
       <li><a href="descarga.php" target="_blank">Descargar Termino</a></li>
      <li><a href="https://www.google.com.py/?gws_rd=ssl" target="_blank">Busqueda</a></li>
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="login1.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav>
    <div class="conten" style="width: 800px; height: 520px;">
        <div class="image actual">
            <img src="imagen/motivacion1.jpg">

        </div>
        <div class="image">
            <img src="imagen/motivacion2.jpg">

        </div>
        <div class="image">
            <img src="imagen/motivacion3.jpg">

        </div>
        <div class="image">
            <img src="imagen/motivacion6.jpg">

        </div>

        <a href="#" class="anterior" onclick="anterior();">&#10094</a>
        <a href="#" class="siguiente" onclick="siguiente();">&#10095</a>
        

        <div class="boton">
            <img src="iconos/pause.png" id="btn" onclick="playpause();" />
        </div>

    </div>
    
</body>
<footer class="container-fluid text-center">
  <p>Global Running</p>
  <p>Fernando de la Mora, zona Sur</p>
  <p>Telefonos:021555.555 Celular:0981555.555</p>
  
</footer>
</html>
