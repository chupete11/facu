<?php require_once ('conexion.php');
$conexion=conectarBD();?>


<html lang="en">
<head>
  <title>Lista de Registrados</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <script src="assets/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <style>
      td{
          width: 100px;
          height: 35px;
      }
      table{
          width: 100%;
          border-color: #000;
           }
           th{
               background-color: #cccccc;
               text-align: center;
           }
      
      
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="mantenimiento.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        <li class="active"><a href="listaderegistrados.php">Lista de Registrados</a></li>
      <li><a href="busqueda1.php">Buscar por Cedula</a></li>
      <li><a href="procesar.php">Actualizar Registros</a></li>
      <li><a href="borrar.php">Borrar Registros</a></li>
      <li><a href="crearevento.php">Cargar Evento</a></li>
      <li><a href="remeras.php">Talla Remera</a></li>
       <li class="nav-item dropdown bg-dark">
        <a class="nav navbar-nav " href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Control Evento
        </a>
        <div class="dropdown-menu  dropdown-item bg-dark">
          <a class="dropdown-item bg-dark" href="controlEvento.php">Estado Evento</a>
          
        </div>
      </li>
      
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span>Salir</a></li>
    </ul>
  </div>
</nav>
    <div class="container">
    <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }
    ?>
    
    <center><h1>Bienvenido Usuario</h1></center>
    <center><?php
    //de esta forma concatenamos el dato de ususario registrado para un saludo personalizado
    echo "Hola: " . $_SESSION["usuario"]. "<br><br>";
        
    ?>
        </center>
    
    
        
        
        
    <?php
    
    $query="select *  from persona order by id_pers";
    $resultado=pg_query($conexion,$query) or die("Error en la consulta");
    $nr=pg_num_rows($resultado);
    if($nr>0){
        echo"<table  border=1  >
        <tr><th>Codigo</th><th>Evento</th><th>Cedula</th><th>Nombre</th><th>Apellido</th><th>FechaNac.</th><th>Correo</th><th>Genero</th><th>Celular</th></tr>";
        while ($filas = pg_fetch_array ($resultado)){
        echo "<td>".$filas["id_pers"]."</td>";
         echo "<td>".$filas["evento"]."</td>";
        echo "<td>".$filas["cedula"]."</td>";
        echo "<td>".$filas["nombre"]."</td>";
        echo "<td>".$filas["apellido"]."</td>";
        echo "<td>".$filas["fecha_nac"]."</td>";
        echo "<td>".$filas["email"]."</td>";
        echo "<td>".$filas["sexo"]."</td>";
        echo "<td>".$filas["celular"]."</td></tr>";
        }echo "</table>";
        
    }else{
        
        echo"no hay datos";
    }
    ?>

    </div>

</body>

</html>
