<?php require_once ('conexion.php');
$conexion=conectarBD();

?>

<html lang="en">
<head>
  <title>Producto</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../bootstrap/bootstrap-3.3.7-dist/css/bootstrap.css">
  <link rel="stylesheet" href="../css/estilo2.css">
  <script src="../assets/jquery-3.3.1.min.js"></script>
  
  <script src="../bootstrap/js/bootstrap.min.js"></script>

</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Global Running</a>
    </div>
      
    <ul class="nav navbar-nav">
      <li><a href="inicio.php">Inicio</a></li>
       <li><a href="AsignarNro.php" >Asignar Nro</a></li>
       <li><a href="kits.php" >Kits Retirados</a></li>
      
      
      <li><a href="https://www.google.com.py/?gws_rd=ssl" target="_blank">Busqueda</a></li>
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span> Salir</a></li>
    </ul>
  </div>
</nav>
    <div class="container">
        <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("location:../login1.php");
    }
        
    ?>

  

    </div>
    
       <div class="container">
           <div class=" container-fluid" >
               <center><h1>Asignar Número</h1></center>
               
               <center><img src="imagkits/imglogin.jpg" style="width: 300px; height: 200px;"></center>
               <br>
               <div class="col-lg-12" >
                   <form method="post" action="#"  >
                   <label style="font-size: 19px;">Cedula Nro:</label>
                   <input type="text" name="cedula" id="cedula" style="width: 300px; height: 60px; font-size: 30px;" required >
                                    
                   <label style="font-size: 19px;">Corredor Nro:</label>
                   <input type="text" name="corredor" id="corredor" style="width: 300px; height: 60px; font-size: 30px;" required>
                   
                                      
                   <input type="button" name="enviar" id="enviar" style="width: 150px; height: 60px; font-size: 20px;" value="Enviar">
                  </form>
                   </div>
               <div class="col-lg-12">
                   
               </div>
                <div class="col-lg-12">
                   
               </div>
               
               
               <div class="col-lg-12">
               <p></p>
                   <table class="table" id="respa">
    <thead>
      <tr>
        <th>Cedula</th>
        <th>Nombre y Apellido</th>
        <th>Talla</th>
        <th>Nro:Corredor</th>
        <th>Fecha Retiro</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
     
    </tbody>
  </table>

    

                   
</div>
               
</div>   


</div>

</body>

<script>
    $(document).ready(function(){
        $("#enviar").click(function(){
          enviar();
        });
});
    
    
    function enviar(){
        var cedula= document.getElementById('cedula').value;
        var corredor= document.getElementById('corredor').value;
        var dataen= 'cedula='+ cedula +'&corredor='+ corredor;
        $.ajax({
            type:'post',
            url:'procesar.php',
            data:dataen,
            success: function(resp){
              var dataR = JSON.parse(resp);
              if(dataR.success == true){
                $("#respa").empty();
                $("#respa").html(dataR.Html);
                $("#cedula").val('');
                $("#corredor").val('');
              }else{
                $("p").val(dataR.Html);
                alert("Numero de Cedula ya se marco como retirado");
              }
            }
         }); 
         return false;
    };
    </script>
</html>