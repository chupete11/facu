<?php
require_once ('conexion.php');

//$evento = $_POST["e"] != null  ? $_POST['e'] :'';
$evento = isset($_POST["evento"]) ? $_POST['evento'] : '';
$cedula = isset($_POST["cedula"]) ? $_POST['cedula'] : '';
$pago = isset($_POST["pago"]) ? $_POST['pago'] : '';

function get_testo($Ci) {
    $conexion = conectarBD();

    $query = "SELECT nombre, apellido, cedula, fecha_nac,direccion,celular,telefono,email,cuidad FROM persona where cedula = '" . $Ci . "'";

    $resut = pg_query($conexion, $query);
    $registros = pg_fetch_object($resut);

    return $registros;
}

function getkm($evento) {
    $con = conectarBD();

    $query = 'select * from categoria where id_evento = ' . $evento;

    $resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
}

function getmonto($id_evento) {
    $conexion = conectarBD();

    $query = "select monto from evento where id_evento=" . $id_evento;
    $resut = pg_query($conexion, $query);
    $data = pg_fetch_object($resut);
    return $data;
}

function getpais() {
    $con = conectarBD();

    $query = 'select descripcion from paises ';

    $resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
}

function gettalla($evento) {
    $con = conectarBD();

    $query = "select * from tallas1 where id_evento=" . $evento;

    $resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
}

function getcuidad($evento) {
    $con = conectarBD();

    $query = "select cuidad from persona where evento=" . $evento;

    $resut = pg_query($con, $query);
    $data = pg_fetch_object($resut);
    return $data;
}

$prueba = get_testo($cedula);
$data['km'] = getkm($evento);
$data['monto'] = getmonto($evento);
$data['pais'] = getpais();
$data['tallas'] = gettalla($evento);
// $data['cuidad'] = getcuidad($evento);

$monto = $data['monto']->monto;
$cuidad  = isset($prueba->cuidad)? $prueba->cuidad : '' ;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es" xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>Formulario Registro</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script src="assets/jquery-3.3.1.min.js"></script>
        <script src="bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css"></link>

    </head>
    <body>
        <div class="container">
            <div class="col-sm-12">
                <div class="well" style="margin-top: 15px;">
                    <h1 class="text-center">Formulario de Registro</h1>
                    <b><hr></b>
                    <form action="procesar1.2.php" method="POST" name="frm">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Nombre</label>
                                <input type="text" class="form-control text-danger" name="nombre" value="<?php echo isset($prueba->nombre) ? $prueba->nombre : '';?>" placeholder="Nombre" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Apellido</label>
                                <input type="text" class="form-control" name="apellido" placeholder="Apellido" value="<?php echo isset($prueba->apellido) ? $prueba->apellido : '';?>" required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Fecha Nac</label>
                                <input type="date" class="form-control" name="fecha_nac" value="<?php echo isset($prueba->fecha_nac) ?$prueba->fecha_nac : '' ;?>" placeholder="Fecha Nacimiento" required >
                                    <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 ">Email</label>
                                <input id="correo" class="form-control" name="Correo" rows="1" value="<?php echo isset($prueba->email) ? $prueba->email : ''; ?>" placeholder="Correo"  required></input>
                                <div class="help-block with-errors"></div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="email" class="h4">Direccion</label>
                                <input type="text" class="form-control" value="<?php echo isset($prueba->direccion) ? $prueba->direccion : ''; ?>" name="direccion" id="direccion" placeholder="Direccion" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="cuidad" class="h4">Cuidad</label>
                                <input type="text" class="form-control"  name="cuidad" id="cuidad"  value="<?php echo $cuidad; ?>" required>
                                    <div class="help-block with-errors"></div>
                            </div>
                                <div class="form-group col-sm-4">
                                <label for="cuidad" class="h4">Cedula</label>
                                <input type="text" class="form-control"  name="cedula" id="cedula"  value="<?php echo $cedula; ?>" required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="text" class="h4">Celular</label>
                                <input type="text" class="form-control" value="<?php echo isset($prueba->celular) ? $prueba->celular : ''; ?>"  name="celular"  placeholder="Celular" required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4">Telefono</label>
                                <input type="text" class="form-control"  name="telefono" value="<?php echo isset($prueba->telefono) ? $prueba->telefono : '';  ?>" id="telefono" placeholder="Telefono" >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4">Distancia</label>
                                <select class="form-control" id="exampleFormControlSelect1" id="categoria"  name="categoria">
                                    <?php foreach ($data['km'] as $d) : ?>
                                        <option value="<?php echo $d->id_cate; ?>"> <?php echo $d->categoria; ?> </option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4">Sexo</label>
                                <select class="form-control" id="exampleFormControlSelect1" id="sexo"  name="sexo">                               
                                    <option value="F">FEMENINO</option>
                                    <option value="M">MASCULINO</option>                   
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for=exampleFormControlSelect1" class="h4">Pais</label>
                                <select class="form-control" id="exampleFormControlSelect1" id="pais"  name="pais">
                                    <?php
                                    foreach ($data['pais'] as $d) :
                                        $select = $d->descripcion == 'Paraguay' ? 'selected="true"' : '';
                                        ?>
                                        <option class="po" value="<?php echo $d->descripcion; ?>" <?php echo $select; ?> > <?php echo $d->descripcion; ?>  </option>
                                <?php endforeach; ?>
                                </select>  
                            </div>
                            <div class="form-group col-sm-4">

                            </div>


                            <div class="form-group col-sm-4">
                                <label for=exampleFormControlSelect1" class="h4">Talla</label>
                                <select class="form-control" id="exampleFormControlSelect1" id="talla"  name="talla" required>
                                    <option class="po" value="" >seleccione talla..</option>
                                    <?php foreach ($data['tallas'] as $d) : ?>
                                        <?php if ($d->saldo > 0): ?>
                                            <option class="po" value="<?php echo $d->tallas; ?>" > <?php echo $d->tallas; ?> </option>
                                         <?php endif ?>
                                    <?php endforeach; ?>                  
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>


                            <div class="form-group col-sm-4">
                                <label for="compro" class="h4">Comprobante</label>
                                <input type="text" class="form-control"  name="compro" id="compro" placeholder="Comprobante" required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            
                            <div class="form-group col-sm-4">
                                <label for="monto" class="h4">Monto</label>
                                <input type="text" class="form-control"  name="monto" id="monto"  value="<?php echo $monto; ?>" disabled >
                                    <div class="help-block with-errors"></div>
                            </div>

                            
                            <input type="hidden"  name="monto1" value="<?php echo $monto; ?>"/>
                            <input type="hidden" name="pago" value="<?php echo $pago; ?>">
                                <input type="hidden" name="evento" value="<?php echo $evento; ?>">
                                    
                                   
                                    <div class="form-group col-sm-4">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <button type="submit" style="margin-top: 5px" id="enviar" class="btn btn-success btn-lg pull-center ">Enviar</button>
                                    </div>
                                    <div class="form-group col-sm-4">
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
        </body>
</html>