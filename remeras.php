<?php
require_once ('conexion.php');
function gettalla() {
    $con = conectarBD();

    $query = "SELECT id_evento, nombre from evento where estado='t'" ;

    $resut = pg_query($con, $query);
    $resulta = array();
    
    while ($data=pg_fetch_object($resut)){
        $resulta[]=$data;
    }
    return $resulta;
}

$id_evento = gettalla();
?>
<html lang="en">
<head>
  <title>Calendario</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <script src="assets/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>

</head>
 <div class="container">
        <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }
        
    ?>
    
   
  

    </div>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="mantenimiento.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        <li ><a class="nav-brand active" href="registrados.php">Lista de Registrados</a></li>
      <li><a href="busqueda1.php">Buscar por Cedula</a></li>
      <li><a href="actualizar.php">Actualizar Registros</a></li>
      
      <li><a href="borrar.php">Borrar Registros</a></li>
      <li><a href="crearevento.php">Cargar Evento</a></li>
      <li><a href="remeras.php">Talla Remera</a></li>
      <li><a href="tallas.php">Saldo Talla</a></li>
     </li>
      <ul class="nav navbar-nav">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Control Evento
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="controlEvento.php">Estado Evento</a></li>
          <li><a href="grafico.php">Grafico</a></li>
          <li><a href="regisUsuario.php">Registrar Usuario</a></li>
        </ul>

      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span>Salir</a></li>
    </ul>
  </div>
</nav>
    <div class="container" >
         <center><h1>Bienvenido Usuario</h1></center>
    <center><?php
    //de esta forma concatenamos el dato de ususario registrado para un saludo personalizado
    echo "Hola: " . $_SESSION["usuario"]. "<br><br>";
        
    ?>
        </center>
         <form action="CargaRemera.php" method="POST" name="frm">
                                <div class="form-group col-sm-12">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label for=exampleFormControlSelect1" class="h4">Evento</label>
                                            <select class="form-control"  id="evento"  name="evento" required>
                                    <option class="po" value="-1" >seleccione Evento...</option>
                                    <?php foreach ($id_evento as $d) : ?>
                                        
                                            <option class="po" value="<?php echo $d->id_evento; ?>" > <?php echo $d->nombre; ?> </option>
                                         
                                    <?php endforeach; ?>                  
                                </select>
                                        </div>
                                        <div class="form-group col-sm-4">
                                        <button type="submit" style="margin-top: 40px" id="enviar" class="btn btn-success btn-ms pull-center ">Enviar</button>
                                    </div>
                                    </div>
                                
                                
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Descripcion</label>
                                <input type="text" class="form-control text-danger" name="desc" id="desc" placeholder="FXP" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Cantidad</label>
                                <input type="number" class="form-control text-danger" name="cant"  id="cant"placeholder="200" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4">Sexo</label>
                                <select class="form-control"  id="sexo"  name="sexo">                               
                                    <option value="101">FEMENINO</option>
                                    <option value="100">MASCULINO</option>                   
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
            
            
            </form>

    </div>

</body>

</html>
