$(document).ready(function() {
    $("#guardar").click(function() {
        insFactura();
    });
});

function insFactura() {
    var montos = $("#Montos").data();
    var fecha = $("#fechaE").data();
    var nroFact = $("#nroFactura").data();
    var formPago = $("input[name='forma_pago']:checked").val();

    $.ajax({
        url: localStorage.url + '/insFacturar.php',
        type: 'POST',
        data: {
            cant: 1,
            descrip: 'por evento realizado segun presupuesto' + montos.idpress,
            monto: montos.subtotal,
            iva: montos.iva,
            nrofactura: nroFact.nro,
            formPago: formPago,
            total: montos.total,
            fechafact: fecha.fecha,
            idpresu: montos.idpress
        },
        success: function(response) {
            var dataR = JSON.parse(response);
            if (dataR.error == false) {
                window.print()
            }
        }
    })
}