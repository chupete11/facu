$(document).ready(function() {
    $("#buscar").click(function() {
        var nroRecibo = $('input[name="buscar"]').val();
        buscarNro(nroRecibo);

    });

    $("#imprimir").click(function() {
        getprint();
    });
});

function buscarNro(data) {
    var info = data;
    $.ajax({
        type: "POST",
        url: localStorage.url + 'secretaria/buscarRecibo.php',
        data: { buscar: data, tipo: 1 },
        beforeSend: function() {
            //imagen de carga
            $("#resultado1").html("<p align='center'><img src='" + localStorage.url + "js/ajax-loader.gif' /></p>");
        },
        error: function() {
            alert("error petición ajax");
        },
        success: function(data) {
            var dataR = JSON.parse(data);
            if (data) {
                $("input[name='nombre']").val(dataR.cliente.nombre + ' ' + dataR.cliente.apellido);
                $("input[name='evento']").val(dataR.cliente.evento);
                $("input[name='talla']").val(dataR.cliente.tallas);
                $("input[name='cedula']").val(dataR.cliente.cedula);
                $("input[name='fecha_nac']").val(dataR.cliente.fecha_nac);
                $("input[name='monto']").val(dataR.cliente.monto);
                $("#imprimir").prop('disabled', false);
            } else {
                $("#sacar").show();
                $("#sacar").val(0)
                $("#pruebas").empty();
            }
        }
    });
}

function getprint() {
    var datos = new FormData();
    datos.append('nombre', $("input[name='nombre']").val());
    datos.append('evento', $("input[name='evento']").val());
    datos.append('talla', $("input[name='talla']").val());
    datos.append('cedula', $("input[name='cedula']").val());
    datos.append('fecha_nac', $("input[name='fecha_nac']").val());
    datos.append('monto', $("input[name='monto']").val());
    $.ajax({
        contentType: false,
        processData: false,
        url: 'http://localhost:8080/runningsys/secretaria/ticket.php',
        type: 'POST',
        data: datos,
        success: function(response) {
            if (response == 1) {
                $("input:text").val('');
            } else {
                alert('Error');
            }
        }
    });
}