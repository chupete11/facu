function getPresupuestoJson() {
    var id = $('#idEvento').val()
    $.ajax({
        url: localStorage.url + 'returnEventjson.php',
        type: 'POST',
        data: { idPresu: id },
        success: function(data, textStatus, jqXHR) {
            var dataR = JSON.parse(data);
            $('#nombre').val(dataR.DescripEvent.evento);
            $('#cantP').val(dataR.DescripEvent.cantPersona);
            $('#lugar').val(dataR.DescripEvent.lugar);
            $('#fecha').val(dataR.DescripEvent.fecha);
            $('#cate').val((dataR.km.toString()));
            // $('#monto').val(dataR.DescripEvent.TotalP);
            selectedSS(dataR.idCliente);

        }
    })
}

function selectedSS(id) {
    var Old = $("#Select1").val();
    $("#Select1 option[value='" + Old + "']").attr("selected", false);
    $("#Select1 option[value='" + id + "']").attr("selected", true);
}

function insEvento() {
    var file = document.getElementById('imag');
    var imgenName = typeof file.files[0] != 'undefined' ? file.files[0].name : '';
    if (imgenName != '') {
        var dataF = new FormData();
        dataF.append('archivo', file.files[0]);
        dataF.append('nombre', $('#nombre').val());
        dataF.append('cant_part', $('#cantP').val());
        dataF.append('estado', 't');
        dataF.append('img', imgenName);
        dataF.append('fecha_event', $('#fecha').val());
        dataF.append('categoria', $('#cate').val());
        dataF.append('lugarEvent', $('#lugar').val());
        dataF.append('descripcion_event', $('#desc').val());
        dataF.append('monto', $('#monto').val());
        dataF.append('idCliente', $('#Select1').val());

        $.ajax({
            url: localStorage.url + 'isnEvento.php',
            type: 'POST',
            contentType: false,
            processData: false,
            data: dataF,
            success: function(data, textStatus, jqXHR) {
                var dataR = JSON.parse(data);

                if (dataR.Error == false) {
                    $('#nombre').val('');
                    $('#cantP').val('');
                    $('#cantP').val('');
                    $('#fecha').val('');
                    $('#cate').val('');
                    $('#lugar').val('');
                    $('#desc').val('');
                    $('#monto').val('');
                    alert(dataR.success)
                    $('#factura').show();
                    $('#enviar').hidden();
                } else {

                }
            }
        })
    } else {
        alert('FAVOR SELECCIONE LA IMAGEN')
    }

}