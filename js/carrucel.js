 var actual = 0;

        function mostrar(n) {
            var imagenes = document.getElementsByClassName("image");
            for (i = 0; i < imagenes.length; i++) {
                if (imagenes[i].className.includes("actual")) {
                    imagenes[i].className = imagenes[i].className.replace("actual", "");
                    break;
                }
            }
            actual = n;
            imagenes[n].className += " actual";
        }

        function siguiente() {
            actual++;
            if (actual > 3) {
                actual = 0;
            }
            mostrar(actual);
        }

        function anterior() {
            actual--;
            if (actual < 0) {
                actual = 3;
            }
            mostrar(actual);
        }
        var velocidad = 2000;
        var play = setInterval("siguiente()", velocidad);

        function playpause() {
            var boton = document.getElementById("btn");

            if (play == null) {

                boton.src = "iconos/pause.png";
                play = setInterval("siguiente()", velocidad);
            } else {
                clearInterval(play);
                play = null;
                boton.src = "iconos/play.png";
            }
        }

  