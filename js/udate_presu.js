$(document).ready(function() {
    $("#sel1").change(function() {
        var idEvento = $(this).val();
        buscarPresu(idEvento);
    });

    $("button").click(function() {
        var idCliente = $('#idClie').val();
        var idEvento = $('#sel1').val();
        var evento = $('#evento').val();
        var fecha = $('#fecha').val();
        var lugar = $('#lugar').val();
        var cantC = $('#cantC').val();
        var tipo = 4;
        var listaOpcion = recorrerLista();
        var listaOpcion2 = recorrerLista2();
        var TotalPagar = cantotal();

        if (idEvento != '' && evento != '' && lugar != '' && cantC > 1) {
            var envio = { idCliente, evento, fecha, lugar, cantC, tipo, listaOpcion, listaOpcion2, TotalPagar, idEvento };
            presUpdate(envio);
        } else {
            alert('complete con los datos obligatorios: cliente, nombre Evento, lugar, cantidad corredor');
        }
    });

});

function buscarPresu(data) {
    $.ajax({
        type: "POST",
        url: localStorage.url + '/presu_controller.php',
        data: { idEvent: data, tipo: 3 },
        dataType: "html",
        success: function(data) {
            var dataR = JSON.parse(data);
            var dataR2 = JSON.parse(dataR.jsonData.descripcion);
            $('#cliente').val(dataR.jsonData.nombrecliente);
            $('#idClie').val(dataR.jsonData.id_cliente);
            $('#evento').val(dataR2.DescripEvent.evento);
            $('#fecha').val(dataR2.DescripEvent.fecha);
            $('#lugar').val(dataR2.DescripEvent.lugar);
            $('#cantC').val(dataR2.DescripEvent.cantPersona);
            Checkest(dataR2.km);
            listCheked(dataR2.items);
            $('#Total').html(format(dataR.jsonData.total_presu));
        }
    });
}

function Checkest(cheks) {
    $("input[name='lista']").attr('checked', false);
    $("input[name='lista']").each(function() {
        var chek = $(this).val();
        var i;
        for (i = 0; cheks.length >= i; i++) {
            if (cheks[i] == chek) {
                $(this).attr('checked', true);
            }
        }
    });
}

function listCheked(list) {
    $("input[name='lista2']").attr('checked', false);
    $("input[name='lista2']").each(function() {
        var check = $(this).val();
        var i;
        for (i = 0; list.length >= i; i++) {
            if (list[i] == check) {
                $(this).attr('checked', true);
            }
        }
    });
}

function format(input) {
    var num = input.replace(/\./g, '');
    if (!isNaN(num)) {
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');
        return num;
    } else {
        return input.replace(/[^\d\.]*/g, '');
    }
}



function presUpdate(data) {
    var even = data.evento;

    $.ajax({
        url: localStorage.url + '/presu_controller.php',
        type: 'POST',
        data: { datos: data, tipo: data.tipo, idPresupuesto: data.idEvento, idCliente: data.idCliente },
        // data:{tipo: data.tipo, Input: data.evento+ '/' + data.fecha + '/'+ data.lugar + '/' + data.cantC, Ckecks1: listaOpcion, Ckecks2: listaOpcion2},
        success: function(data, textStatus, jqXHR) {
            var info = JSON.parse(data);
            if (info.success > 0) {
                alert('Datos actualizados Correctamente');
                $('#evento').val('');
                $('#fecha').val('');
                $('#lugar').val('');
                $('#cantC').val('');
                $('button').hide();
                $('#pdf').show();
            } else {
                alert('Error no pudo Actualizar los datos');
            }
        }
    });
}