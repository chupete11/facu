$(document).ready(function() {
    $("#busqueda").focus;

    $("#busqueda").keyup(function(e) {
        var consulta = $("#busqueda").val();
        buscarClient(consulta);
    });

    $("button").click(function() {
        var idCliente = valiCliente();
        var evento = $('#evento').val();
        var fecha = $('#fecha').val();
        var lugar = $('#lugar').val();
        var cantC = $('#cantC').val();
        var tipo = 1;
        var listaOpcion = recorrerLista();
        var listaOpcion2 = recorrerLista2();
        var TotalPagar = cantotal();

        if (idCliente != '' && evento != '' && lugar != '' && cantC > 1) {
            var envio = { evento, fecha, lugar, cantC, tipo, listaOpcion, listaOpcion2, TotalPagar, idCliente };
            presupuesto(envio)
        } else {
            alert('complete con los datos obligatorios: cliente, nombre Evento, lugar, cantidad corredor');
        }
    });

    $("input[name = 'cantprecio']").focusout(function() {
        var PrecioTotal = cantotal();
        $("#Total").html(format(PrecioTotal.toString()));
    });

    $("input[name=lista2]").click(function() {
        var PrecioTotal = cantotal();
        $("#Total").html(format(PrecioTotal.toString()));
    });


});

function presupuesto(data) {
    var even = data.evento;

    $.ajax({
        url: localStorage.url + '/presu_controller.php',
        type: 'POST',
        data: { datos: data, tipo: data.tipo, idCliente: data.idCliente },
        // data:{tipo: data.tipo, Input: data.evento+ '/' + data.fecha + '/'+ data.lugar + '/' + data.cantC, Ckecks1: listaOpcion, Ckecks2: listaOpcion2},
        success: function(data, textStatus, jqXHR) {
            $('#evento').val('');
            $('#fecha').val('');
            $('#lugar').val('');
            $('#cantC').val('');
            $('button').hide();
            $('#pdf').show();
        }
    });
}

function recorrerLista() {
    var listaCompras = '';
    $("input[name=lista]").each(function(index) {
        if ($(this).is(':checked')) {
            listaCompras += +$(this).val() + '/';
        }
    });
    return listaCompras;
}

function recorrerLista2() {
    var listaCompras = '';
    $("input[name=lista2]").each(function(index) {
        if ($(this).is(':checked')) {
            listaCompras += +$(this).val() + '/';
        }
    });
    return listaCompras;
}

function cantotal() {
    var precio = 0;
    var cantPersona = ($('#cantC').val() * $('input[name=valorPersona]').val()) - 5000;
    $("input[name=lista2]").each(function(index) {
        if ($(this).is(':checked')) {
            precio += $(this).data('precio');
        }
    });
    return precio + cantPersona;
}


function buscarClient(data) {
    $.ajax({
        type: "POST",
        url: localStorage.url + '/presu_controller.php',
        data: { letra: data, tipo: 2 },
        dataType: "html",
        beforeSend: function() {
            //imagen de carga
            $("#resultado1").html("<p align='center'><img src='" + localStorage.url + "js/ajax-loader.gif' /></p>");
        },
        error: function() {
            alert("error petición ajax");
        },
        success: function(data) {
            if (data) {
                $("#sacar").hide();
                $("#sacar").val(1)
                $("#pruebas").html(data);
            } else {
                $("#sacar").show();
                $("#sacar").val(0)
                $("#pruebas").empty();
            }
        }
    });
}

function valiCliente() {
    var cliente = $("#sel1").val();
    if (cliente == undefined) {
        var valid = $('#sacar').val();
        cliente = valid == 0 ? '' : document.getElementById('busqueda1').value;
        return cliente;
    }

    return cliente;
}

function format(input) {
    var num = input.replace(/\./g, '');
    if (!isNaN(num)) {
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');
        return num;
    } else {
        return input.replace(/[^\d\.]*/g, '');
    }
}