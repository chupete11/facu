<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tesis2018</title>
</head>

<body>
    
<?php

    try{
    // nos conectamos a la base de datos por medio de la libreria PDO
    $base=new PDO("pgsql:host=localhost;port=5432;dbname=facu;user=postgres;password=postgres");
    $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $sql="Select * from usuario where usuario= :login AND contrasena= :password";
    //ejecutamos la consulta 
        $resultado=$base->prepare($sql);
    //htmlentities nos permite eliminar la inyeccion SQL
    $login=htmlentities(addslashes($_POST['login']));
    $password=htmlentities(addslashes(md5($_POST['password'])));
    //binvalue nor permite comparar los datos que enviamos con los datos de la base de datos 
    $resultado->bindValue(":login", $login);
    $resultado->bindValue(":password", $password);
    //ejecutamos 
    $resultado->execute();
    
    $numero_registro=$resultado->rowCount();
    $dato=  $resultado->fetch(PDO::FETCH_OBJ);
    
    if($numero_registro !=0){
         // de esta manera iniciamos la seccion para el ususario registrado  
       session_start(); 
        // aqui almacenamos los datos del usuario registrado
        $_SESSION["usuario"]=$_POST["login"];
        
        switch ($dato->tipousuario){
            
            case 'admin':
                header("location:mantenimiento.php");
            break; 
        case 'secretaria':
                header("location:secretaria/cliente.php");
            break;
        case 'administracion':
                header("location:administracion/inicio.php");
            break;
        case 'usuario':
                header("location:usuario/inicio.php");
            break;
        }
        
        
        
        
        
        
    }else{
        // con la etiqueta header le direccionamos siempre a la pagina de login
        header("location:login1.php");
    }
    
    
    
    
}catch(exception $e){
    
    die("Error: ". $e->getMessage());
}

    
    
?>
    
    
    
</body>
</html>