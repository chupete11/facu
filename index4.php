<?php

?>
<!doctype html>
<html lang="en">
  <head>
        <title>Inicio</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script src="assets/jquery-3.3.1.min.js"></script>
        <script src="bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css"></link>
        <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
          
    </head>
    <div class="container-fluid">
    <nav class="navbar navbar-inverse">
  
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Inicio</a></li>
      <li><a href="#">Page 1</a></li>
      <li><a href="#">Page 2</a></li>
      <li><a href="#">Page 3</a></li>
      <li><a href="#">Page 4</a></li>
      <li><a href="#">Page 5</a></li>
      <li><a href="#">Page 6</a></li>
    </ul>
        
        <ul class="nav navbar-nav navbar-right">
<!--      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>-->
      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
        </nav>
  </div>

 
  <body>
  <div class="container-fluid">
	<div class="row" >
		<div class="col-md-12">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">

  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  
  <div class="carousel-inner">
    <div class="item active">
        <img src="imagen/fondo33.jpg" width="1400" height="400"alt="Los Angeles">
    </div>

    <div class="item">
        <img src="imagen/motivacion6.jpg" width="1400" height="400"alt="Chicago">
    </div>

    <div class="item">
        <img src="imagen/motivacion2.jpg" width="1400" height="400" alt="New York">
    </div>
       <div class="item">
           <img src="imagen/corredores-1.jpg" width="1400" height="400"alt="Los Angeles">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
		</div>
	</div>
  </div>

      
  
  
  
  </body>
  <footer class="container-fluid text-center">
  <p>Global Running</p>
  <p>Fernando de la Mora, zona Sur</p>
  <p>Telefonos:021555.555 Celular:0981555.555</p>
</footer>
</html>