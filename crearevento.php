<?php require_once ('conexion.php');


function cliente() {
    $con = conectarBD();
    $query = 'SELECT  *  from  cliente';
    $resut = pg_query($con, $query);
    $resulta = array();
    while ($data = pg_fetch_object($resut)) {
        $resulta[] = $data;
    }
    return $resulta;
}

function getPrsupuesto(){
    $con = conectarBD();
    $query = 'SELECT * from get_eventojson;';
    $dataR = pg_query($con, $query);
    $resul = array();
    while ($data = pg_fetch_object($dataR) ){
        $resul[] = $data;
    }
    return $resul;
}
$dataevenlist = getPrsupuesto();
$data['nombre_clien'] = cliente();
?>

<html lang="en">
<head>
  <title>Crear Evento</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css"
  
  <script src="js/carrucel.js"></script>
<!--  <link rel="stylesheet" href="css/estilo2.css"></link>-->
  <script src="assets/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <script>
    localStorage.url = 'http://localhost:8080/runningsys/';
  </script>

</head>
<body>


<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="mantenimiento.php">Global Running</a>
    </div>
    <ul class="nav navbar-nav">
        <li ><a class="nav-brand active" href="registrados.php">Lista de Registrados</a></li>
      <li><a href="busqueda1.php">Buscar por Cedula</a></li>
      <li><a href="actualizar.php">Actualizar Registros</a></li>
      
      <li><a href="borrar.php">Borrar Registros</a></li>
      <li><a href="crearevento.php">Cargar Evento</a></li>
      <li><a href="remeras.php">Talla Remera</a></li>
      <li><a href="tallas.php">Saldo Talla</a></li>
     </li>
      <ul class="nav navbar-nav">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Control Evento
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="controlEvento.php">Estado Evento</a></li>
          <li><a href="grafico.php">Grafico</a></li>
          <li><a href="regisUsuario.php">Registrar Usuario</a></li>
        </ul>

      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="cierre_seccion.php"><span class="glyphicon glyphicon-log-in"></span>Salir</a></li>
    </ul>
  </div>
</nav>

     <?php
    session_start();
    if(
    !isset($_SESSION["usuario"])){
        header("Location:login1.php");
    }
        
    ?>
    
    <center><h1>Bienvenido Usuario</h1></center>
    <center><?php
    //de esta forma concatenamos el dato de ususario registrado para un saludo personalizado
    echo "Hola: " . $_SESSION["usuario"]. "<br><br>";
        
    ?>
       <div class="container">
            <div class="col-sm-12">
                <div class="well" >
                    <h1 class="text-center">Crear Evento </h1>
                    <div class="row">
                        <div class="col-sm-3">
                        <label class="test text-right h4">Evento Presupuestado</label>
                        <select onchange="getPresupuestoJson()" class="form-control" id="idEvento"   name="idEvento">
                             <option>selecione even..</option>
                             <?php foreach ($dataevenlist as $d) :?>                          
                                 <option class="po" value="<?php echo $d->id_pres; ?>"  > <?php echo $d->evento; ?>  </option>
                         <?php endforeach; ?>
                         </select>
                        </div>
                    </div>
                    <b><hr></b>
                    <form action="procesarEvento.php" method="POST" name="frm">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Nombre del Evento</label>
                                <input type="text" class="form-control text-danger"  id="nombre"  name="nombre" placeholder="Nombre del Evento" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Cantidad de participantes</label>
                                <input type="text" class="form-control"  id="cantP" name="cantP"  placeholder="Cantidad"  required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="text" class="h4">Lugar del Evento</label>
                                <input type="text" class="form-control"  id="lugar" name="lugar"  placeholder="Lugar del Evento" required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="text" class="h4">Fecha de Evento</label>
                                <input type="date" class="form-control"  id="fecha" name="fecha"   required>
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="exampleFormControlSelect" class="h4">Estado del Evento</label>
                                <select class="form-control" id="exampleFormControlSelect" id="estado"  name="estado">                               
                                    <option value="t">Publicar</option>
                                    <option value="f">No Publicar</option>                   
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                            
                            
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 ">Categoria</label>
                                <input class="form-control" name="cate" rows="1" id="cate"  placeholder="5km, 10km"  required></input>
                                <div class="help-block with-errors"></div>
                               
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="file" class="h4 ">Imagen del Evento</label>
                                <input class="form-control" type="file" name="imag" rows="1" id="imag"  ></input>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 ">Descripcion</label>
                                <input type="text" class="form-control" name="desc" rows="1" id="desc"  placeholder="Descripcion"  required></input>
                                <div class="help-block with-errors"></div>
                               
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 ">Monto</label>
                                <input class="form-control"  type="number" name="monto" rows="1" id="monto"   required></input>
                                <div class="help-block with-errors"></div>
                               
                            </div>
                            <div class="form-group col-sm-4">
                                <label for=exampleFormControlSelect1" class="h4">Cliente</label>
                                <select class="form-control" id="Select1"   name="cliente">
                                    <?php foreach ($data['nombre_clien'] as $d) :?>                          
                                        <option class="po" value="<?php echo $d->id_clie; ?>" > <?php echo $d->nombre_clien; ?>  </option>
                                <?php endforeach; ?>
                                </select>  
                            </div>                                                   
                                    <div class="form-group col-sm-4">
                                        <button type="button" onclick="insEvento()"style="margin-top: 30px" id="enviar" class="btn btn-success btn-lg pull-center ">Registrar</button>
                                        
                                    </div>
                                    
                            </form>
                    </div>
                        </div>
                    </div>
                </div>
</body>
<footer class="container-fluid text-center">
  <p>Global Running</p>
  <p>Fernando de la Mora, zona Sur</p>
  <p>Telefonos:021555.555 Celular:0981555.555</p>
</footer>
<script src="js/eventoCreator.js"></script>
</html>
